
FILE_SH="slurm/run_script_arr.sh"
CMD="sbatch"
FLAGS="--parsable --qos standby"
FLAGS_SINGLE="${FLAGS} --array 0-0"
FLAGS_ARR="${FLAGS} --array 0-71"

# rescale gridded HYDE population data for ISIMIP3a
${CMD} ${FLAGS_ARR} ${FILE_SH} scale_hyde_pop pop 5arcmin

for model in H08 ER11; do
    J_WIND=$(${CMD} ${FLAGS_ARR} ${FILE_SH} compute_winds ${model})
    J_WIND_MAX=$(${CMD} ${FLAGS_ARR} -d afterany:${J_WIND} ${FILE_SH} storm_max ${model})

    for soc in histsoc 2015soc; do
        J_AREA=$(${CMD} ${FLAGS_ARR} -d afterany:${J_WIND_MAX} ${FILE_SH} affected area ${soc} ${model})
        J_POP=$(${CMD} ${FLAGS_ARR} -d afterany:${J_WIND_MAX} ${FILE_SH} affected pop ${soc} ${model})
        J_AFF=$(${CMD} ${FLAGS_SINGLE} -d afterany:${J_AREA},${J_POP} ${FILE_SH} affected_to_csv ${soc} ${model})
    done

    J_ERA5=$(${CMD} ${FLAGS_ARR} -d afterany:${J_WIND} ${FILE_SH} era5_rainfields ${model})
    J_ERA5_MAX=$(${CMD} ${FLAGS_ARR} -d afterany:${J_ERA5} ${FILE_SH} storm_max era5 ${model})
done

# J_WIND is set in the loop, hence this will refer to the last definition (which is fine)
J_ERA5_TR=$(${CMD} ${FLAGS_ARR} -d afterany:${J_WIND} ${FILE_SH} era5_alongtrack)
for model in H08 ER11; do
    J_RAIN=$(${CMD} ${FLAGS_ARR} -d afterany:${J_ERA5_TR} ${FILE_SH} compute_rain ${model})
    J_RAIN_MAX=$(${CMD} ${FLAGS_ARR} -d afterany:${J_RAIN} ${FILE_SH} storm_max rain ${model})
done
