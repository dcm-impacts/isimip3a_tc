
# Tropical cyclone wind and rain footprints for ISIMIP3a

Scripts to compute time-dependent wind and rain footprints for historical tropical cyclones (TCs) and store in NetCDF
files for use in the ISIMIP3a framework.

## Installation and prerequisites

The scripts have the same requirements as the Python package CLIMADA, so the fastest way to set up a working environment
is by following the "Advanced Instructions" in
the [installation guide of CLIMADA](https://climada-python.readthedocs.io/en/stable/guide/install.html).
As of December 2023, the module for the TC rain model is not yet included in a release version of CLIMADA.
To run the rain-related code in this repository, you need to check out the `develop` branch of
the "climada_petals" git repository.

After installing CLIMADA, run `pip install -e ./` in this repository (from within the conda environment that you
created during the installation of CLIMADA).

Unfortunately, as of April 2024, after setting up the environment, there might still be some warnings, or even errors.
Some of the warnings cannot be avoided, but also do not cause any harm. You can avoid some of the warnings by selecting
other versions of packages, such as a numba=0.57. But the most important part is about errors related to parallelization
support. You should make sure that you don't have "nompi" versions of packages. Run `conda list | grep nompi` to get a
list of packages that are installed without support for parallelization. For example `h5py`. Then, force conda to use
a version of the package with parallelization support, e.g. by running `conda install "h5py=*=mpi*"`. Other packages that
are typically affected by this are `hdf5` and `libnetcdf`.

### Alternative installation approach (step-by-step)

Alternative step-by-step instructions (that worked in November 2023) are as follows:

```shell
$ conda create -n climada_env python=3.11
$ conda activate climada_env
(climada_env) $ conda install netcdf4 xarray pandas eccodes gitpython
# libnetcdf 4.9.2 has a bug, revert to libnetcdf 4.8
(climada_env) $ conda install libnetcdf=4.8
(climada_env) $ git clone git@gitlab.pik-potsdam.de:tovogt/isimip3a_tc.git
(climada_env) $ cd isimip3a_tc
(climada_env) $ pip install -e .
(climada_env) $ cd ..
(climada_env) $ git clone https://github.com/CLIMADA-project/climada_petals
(climada_env) $ cd climada_petals
(climada_env) $ git checkout develop
(climada_env) $ pip install -e .
```

## Inputs

If you work on the PIK cluster, you can obtain all required input files from
`/p/projects/isimip/isimip/tovogt/isimip3a_tc/input/`. For external users, we describe all of the input files in this
section.

### Elevation data

The calculation of rain footprints relies on elevation data that needs to be placed in `./input/srtm15_v2.3_300as.tif`.
You can create this file from the [SRTM15+](https://topex.ucsd.edu/pub/srtm15_plus/) data set using the `gdalwarp`
command with options `-tr 0.083333333333333 -0.083333333333333 -r average`.

### Population data

For the socioeconomic analysis (people affected by TC winds), we need gridded population data from HYDE, the national
annual population counts from ISIMIP3a, and the ISIMIP3a fractional country masks in the following locations:

```
input/pop/hyde_v3.3_popc_1850_2021_5arcmin.nc
input/pop/population_histsoc_national_annual_1950_2021.csv
input/pop/countrymasks_fractional_5arcmin.nc
```

### ERA5 data

The rain-related parts of this repository require input data from ERA5. The repository includes a script to download the
necessary ERA5 data from the Copernicus Climate Data Store (CDS) in `tc_data.era5_download`, but the total amount of
ERA5 data is almost 3 TB. Hence, make sure to download the data only if you really know what you are doing. You might
have an alternative way of accessing the data. For example, there is some ERA5 data stored on the PIK cluster
in `/p/projects/climate_data_central/reanalysis/` (and in `/p/projects/isimip/isimip/tovogt/isimip3a_tc/input/`).

The TC rain model uses variables that are not included in the best-track data, but need to be extracted from ERA5
reanalysis data. Place them in the following locations:

```
input/era5_u850/u_component_of_wind_850_ERA5_<year><month>_hourly.nc
input/era5_v850/v_component_of_wind_850_ERA5_<year><month>_hourly.nc
input/era5_t600/temperature_600_ERA5_<year><month>_hourly.nc
```

For evaluation purposes, this repository comes with scripts to extract TC rain footprints from ERA5 and ERA5-Land. To
use these scripts, place precipitation data in hourly resolution in the following locations:

```
input/era5_pr/total_precipitation_ERA5_<year><month>_hourly.nc
input/era5l_pr/total_precipitation_ERA5-Land_<year><month><day>-<year><month><day>.nc
```

Note that, in spite of the quite ambiguous naming scheme, the ERA5-Land files are supposed to cover only a single month
each. This is the naming scheme of the ERA5-Land files in `/p/projects/climate_data_central/` on the PIK cluster.

## Outputs

All wind and rain footprints will be stored as NetCDF files in subdirectories of the `output` directory.

## Usage

Usage examples:

```shell
(climada_env) $ # compute wind footprints for the 18th year in the considered period (1950-2021), i.e. 1967
(climada_env) $ # H08 indicates that the Holland wind profile is used
(climada_env) $ python -m tc_data.compute_winds H08 17
```

```shell
(climada_env) $ # before computing rain footprints, extract additional ERA5 variables along track
(climada_env) $ python -m tc_data.era5_alongtrack 17
(climada_env) $ # compute rain footprints (with Emanuel-Rotunno wind profile)
(climada_env) $ python -m tc_data.compute_rain ER11 17
```

```shell
(climada_env) $ # for comparison, extract rain footprints from ERA5 precipitation
(climada_env) $ python -m tc_data.era5_rainfields H08 17
```

```shell
(climada_env) $ # aggregate over the storm life time
(climada_env) $ python -m tc_data.storm_max H08 17
(climada_env) $ python -m tc_data.storm_max rain H08 17
(climada_env) $ python -m tc_data.storm_max era5 H08 17
```

```shell
(climada_env) $ # rescale the gridded HYDE population data with ISIMIP3a national population counts
(climada_env) $ python -m tc_data.scale_hyde_pop pop 5arcmin 17
(climada_env) $ # note that only 5arcmin and 30arcmin (0.5 degrees) are implemented, and the purpose of 30arcmin is to
(climada_env) $ # compare the implementation of rescaling with the rescaled population data included in ISIMIP3a.
```

```shell
(climada_env) $ # compute affected area (or population) with a selection of wind thresholds
(climada_env) $ python -m tc_data.affected area H08 17
(climada_env) $ python -m tc_data.affected pop ER11 17
```

```shell
(climada_env) $ # only after running the above lines for all years:
(climada_env) $ # combine all information about affected exposures into a single CSV file
(climada_env) $ python -m tc_data.affected_to_csv ER11
(climada_env) $ python -m tc_data.affected_to_csv H08
```

## Usage in a cluster environment (SLURM)

Obtaining the full ISIMIP3a data set requires a lot of computational and storage resources. That's why you would
typically run the code in a high-performance cluster (HPC) environment. An example SLURM sbatch script that runs one of
the scripts listed in the previous section on SLURM is included in `slurm/run_script_arr.sh.template`. Copy the template
to `slurm/run_script_arr.sh`, open the file in a text editor, and follow the instruction in the file to replace all of
the placeholders according to your environment. Then, submit the script to SLURM as follows:

```shell
(climada_env) $ # compute rain footprints (with Emanuel-Rotunno wind profile) [for years 1955-1967, one job per year]
(climada_env) $ sbatch --array 5-17 slurm/run_script_arr.sh compute_rain ER11
```

An example of how to run not only a single script from the previous section, but the whole data processing pipeline on
a SLURM cluster is included in the script `sbatch_pipeline.sh`.
