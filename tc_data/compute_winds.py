
import os
import sys

from climada.hazard import TCTracks, TropCyclone
import numpy as np
import scipy.sparse
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.data as u_data
import tc_data.util.grid as u_grid
import tc_data.util.io as u_io


OUTPUT_FILE = lambda model, sid: (
    u_const.OUTPUT_DIR / f"wind_{model.lower()}"
    / f"{model.lower()}_obsclim_historical_wind_{sid}.nc"
)


def compute_winds_for_tracks(tracks, centroids, mask_centr_filter, model, pool):
    """Compute (and store) the wind fields for a set of TC tracks

    Parameters
    ----------
    tracks : TCTracks
        A set of TC tracks.
    centroids : Centroids
        Centroids where to model the TC winds.
    mask_centr_filter : ndarray
        Subselection of centroids to use.
    model : str
        Parametric wind field model to use.
    pool : pathos.pool
        Pool that will be used for parallel computation of wind fields.
    """
    haz = TropCyclone.from_tracks(
        tracks,
        centroids=centroids.select(sel_cen=mask_centr_filter),
        model=model,
        store_windfields=True,
        intensity_thres=u_const.MINWIND_MS,
        max_latitude=89.999,
        max_dist_eye_km=u_const.MAX_STORM_SIZE_KM,
        max_dist_inland_km=1e5,
        metric="geosphere",
        pool=pool,
    )
    for track, inten, field in zip(tracks.data, haz.intensity, haz.windfields):
        path = OUTPUT_FILE(model, track.attrs['sid'])
        inten, field = u_grid.unfilter_centroids(mask_centr_filter, inten, field, vectorial=True)
        ds = u_grid.sparse_field_to_ds(
            "wind", field, track["time"].values, centroids, inten=inten, vectorial=True,
        )
        u_io.write_safely(ds, path)


def compute_winds(year, model):
    """Compute (and store) the wind fields for all IBTrACS entries of a year

    Parameters
    ----------
    year : int
        Year for which to extract IBTrACS entries.
    model : str
        Parametric wind field model to use.
    """
    # define a process pool (optional, for execution on a SLURM cluster)
    pool = None
    if 'OMP_NUM_THREADS' in os.environ and int(os.environ['OMP_NUM_THREADS']) > 1:
        from pathos.pools import ProcessPool as Pool
        # we reserve two cores per thread, since we need at least 8 GB memory per thread
        num_threads = int(os.environ['OMP_NUM_THREADS']) // 2
        print(f"Running with {num_threads} threads.")
        pool = Pool(nodes=num_threads)

    print(f"Processing TCs for year {year}.")
    tr = u_data.ibtracs_by_year(year, time_step_h=1)
    tr.data = [
        t for t in tr.data
        if not u_io.path_exists_safely(OUTPUT_FILE(model, t.attrs['sid']))
    ]
    if len(tr.data) == 0:
        print("Nothing to do, all windfield files exist already.")
        return

    cen, dist_coast = u_data.hyde_centroids()
    # ignore pixels that are more than 50 km off shore
    mask_centr_filter = (dist_coast <= 50 * u_const.KM_TO_M)

    chunksize = 8
    for chunk_start in range(0, tr.size, chunksize):
        chunk = tr.data[chunk_start:chunk_start + chunksize]
        compute_winds_for_tracks(TCTracks(data=chunk), cen, mask_centr_filter, model, pool)


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    model = "H08"
    if "ER11" in sys.argv[1:]:
        model = "ER11"

    period = u_const.ISIMIP3A_HIST_PERIOD

    # 72 configurations
    year = np.arange(period[0], period[1] + 1)[task]
    compute_winds(year, model)


if __name__ == "__main__":
    main()
