
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.io as u_io


TRACKS_DIR = u_const.OUTPUT_DIR / "tracks_hist"

VARIABLE_ATTRS = {
    "sid": {
        "description": "IBTrACS Serial ID",
        **u_const.NC_ATTR["sid"],
    },
    "time": {
        "description": "Time associated with a given location of the storm centre",
        **u_const.NC_ATTR["time"],
    },
    "lat": {
        "description": (
            "Latitudinal coordinate of storm centre (as defined by the official WMO agency)"
        ),
        **u_const.NC_ATTR["lat"],
    },
    "lon": {
        "description": (
            "Longitudinal coordinate of storm centre (as defined by the official WMO agency)"
        ),
        **u_const.NC_ATTR["lon"],
    },
    "basin": {
        "description": (
            "Ocean basin: EP=East Pacific, NA=North Atlantic, NI=North Indian, SA=South Atlantic,"
            " SI=South Indian, SP=South Pacific, WP=Western Pacific"
        ),
        "standard_name": "basin",
        "long_name": "Current basin",
        "units": "two-letter abbreviation",
    },
    "central_pressure": {
        "description": "Minimum central pressure from official WMO agency",
        "standard_name": "pres",
        "long_name": "Central pressure",
        "units": "hPa",
    },
    "environmental_pressure": {
        "description": "Environmental pressure (pressure of the outermost closed isobar)",
        "standard_name": "penv",
        "long_name": "Environmental pressure",
        "units": "hPa",
    },
    "max_sustained_wind": {
        "description": "Maximum 1-minute sustained wind speed from official WMO agency",
        **u_const.NC_ATTR["windspatialmax"],
    },
    "nature": {
        "description": (
            "Nature of the cyclone: NR=Not Reported, DS=Disturbance, TS=Tropical System,"
            " ET=Extratropical System, SS=Subtropical System,"
            " MX=Mixed (occurs when agencies reported inconsistent types)"
        ),
        "standard_name": "nature",
        "long_name": "Nature of the cyclone",
        "units": "two-letter abbreviation",
    },
    "radius_max_wind": {
        "description": "Radius of maximum wind speeds",
        "standard_name": "rmw",
        "long_name": "Radius of maximum wind speeds",
        "units": "nautical miles",
    },
    "radius_oci": {
        "description": "Maximum 1-minute sustained wind speed from official WMO agency",
        "standard_name": "roci",
        "long_name": "Radius of the outermost closed isobar",
        "units": "nautical miles",
    },
    "u850": {
        "description": "Wind speed on the 850 hPa pressure level, zonal direction, eastward",
        "standard_name": "u850",
        "long_name": "U component of wind",
        "units": "m s-1",
    },
    "v850": {
        "description": "Wind speed on the 850 hPa pressure level, meridional direction, northward",
        "standard_name": "v850",
        "long_name": "V component of wind",
        "units": "m s-1",
    },
    "t600": {
        "description": "Temperature on the 600 hPa pressure level",
        "standard_name": "t600",
        "long_name": "Temperature",
        "units": "K",
    },
}

VARIABLE_NAMES = {
    "sid": "sid",
    "time": "time",
    "lat": "lat",
    "lon": "lon",
    "basin": "basin",
    "central_pressure": "pres",
    "environmental_pressure": "penv",
    "max_sustained_wind": "windspatialmax",
    "nature": "nature",
    "radius_max_wind": "rmw",
    "radius_oci": "roci",
    "u850": "u850",
    "v850": "v850",
    "t600": "t600",
}


def main():
    period = u_const.ISIMIP3A_HIST_PERIOD
    out_path = TRACKS_DIR / f"tracks_obsclim_historical_{period[0]}_{period[1]}.nc"
    files = [TRACKS_DIR / f"{y}.nc" for y in range(period[0], period[1] + 1)]
    ds = xr.open_mfdataset(files, combine="nested", concat_dim="storm")

    # these are not used, but we still check to be sure that all units agree
    assert (ds["max_sustained_wind_unit"] == "kn").all()
    assert (ds["central_pressure_unit"] == "mb").all()

    ds = ds[list(VARIABLE_ATTRS.keys())]
    for v, a in VARIABLE_ATTRS.items():
        ds[v].attrs = a
        if ds[v].dtype == "object":
            ds[v] = ds[v].fillna("").astype(str)
    ds = ds.rename_vars({v: n for v, n in VARIABLE_NAMES.items()})

    ds.attrs["title"] = "Tropical cyclone tracks based on historical best track data"
    ds.attrs["references"] = u_const.NC_ATTR_GLOBAL["references"] + " " + (
        "The main source dataset (IBTrACS) is described in"
        " Knapp et al. 2010 <https://doi.org/10.1175/2009BAMS2755.1>."
    )

    encoding = {v: {"missing_value": ""} for v in ["basin", "nature"]}
    u_io.write_safely(ds, out_path, encoding=encoding)


if __name__ == "__main__":
    main()
