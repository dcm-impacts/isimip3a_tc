
import os
import sys

import numpy as np
import pandas as pd
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.data as u_data
import tc_data.util.grid as u_grid
import tc_data.util.io as u_io


def load_storm_data(storm_id, ds_grid, model):
    """Load gridded wind and rain (ERA5) data for a single TC

    Parameters
    ----------
    storm_id : str
        The IBTrACS storm ID of the event.
    ds_grid : xr.Dataset or xr.DataArray
        Array or data set with dimensions "lat" and "lon" (in any order).
    model : str
        Load the wind field data that uses this TC wind model.

    Returns
    -------
    DataFrame
    """
    ds_wind, _ = u_data.dense_wind(storm_id, model)
    if ds_wind is None:
        return
    ds_wind["wind"].values[ds_wind["wind"].values == 0] = np.nan
    da_wind = u_grid.reproject_da_to_ref(ds_wind["wind"], ds_grid, resampling="average")
    df_wind = da_wind.to_series().reset_index()
    df_wind = df_wind[df_wind["wind"] > u_const.MINWIND_MS].reset_index(drop=True)
    df_wind["wind"] /= u_const.KN_TO_MS

    ds_rain, _ = u_data.dense_rain_era5(storm_id)
    ds_rain["rain"].values[ds_rain["rain"].values == 0] = np.nan
    da_rain = u_grid.reproject_da_to_ref(ds_rain["rain"], ds_grid, resampling="average")
    df_rain = da_rain.to_series().reset_index()
    df_rain = df_rain[df_rain["rain"] > 0].reset_index(drop=True)

    ds_track = xr.open_dataset(u_const.CACHE_DIR / "tracks_hist_1h" / f"{storm_id[:4]}.nc")
    df_track = (
        ds_track
        .sel(storm=(ds_track.sid == storm_id))
        .squeeze()[["time", "nature"]]
        .to_dataframe()
        .reset_index()[["time", "nature"]]
    )

    return (
        pd.merge(df_wind, df_rain, how="left", on=["time", "lat", "lon"])
        .fillna(0)
        .rename(columns=dict(wind="windspeed_kn", rain="rainfall_mm"))
        .merge(df_track, how="left", on="time")
    )


def tcdata_to_csv(year, model):
    """Store hourly TC hazard data for the given year in CSV files

    The result is written to CSV files, one file per TC.

    Parameters
    ----------
    year : int
        Year for which to process TCs.
    model : str
        Parametric wind field model to consider.
    """
    ds_grid_05d = u_grid.regular_grid(0.5 * u_const.DEG_TO_AS)
    wind_dir = u_const.OUTPUT_DIR / f"wind_{model.lower()}"
    csv_dir = wind_dir / "csv"
    paths = sorted(wind_dir.glob(f"{model.lower()}_obsclim_historical_wind_{year}*.nc"))
    for path in paths:
        storm_id = path.stem.split("_")[-1]
        df = load_storm_data(storm_id, ds_grid_05d, model)
        if df is None:
            continue
        path = csv_dir / f"{storm_id}.csv"
        u_io.write_safely(df, path)


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    model = "H08"
    if "ER11" in sys.argv[1:]:
        model = "ER11"

    period = u_const.RECENT_HIST_PERIOD

    # 40 configurations
    year = np.arange(period[0], period[1] + 1)[task]
    tcdata_to_csv(year, model)


if __name__ == "__main__":
    main()