
import climada.util.coordinates as u_coord
import dask
import numpy as np
import rasterio
import scipy.sparse
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.io as u_io


def regular_grid(res_as):
    """Set up (or load) a global lat/lon grid at the specified resolution

    Parameters
    ----------
    res_as : numeric
        The resolution in arc-seconds. This is rounded to an integer value for reproducibility.

    Returns
    -------
    ds : xr.Dataset
        The data set has dimensions "lat" and "lon" at the specified resolution, and a variable
        "dist_coast" containing the signed distance (in m) of each grid cell to the coastline.
    """
    path = u_const.OUTPUT_DIR / f"grid_{res_as:.0f}as.nc"
    if u_io.path_exists_safely(path):
        return xr.open_dataset(path)

    res_deg = int(res_as) / 3600
    lat_dim = np.arange(-90 + 0.5 * res_deg, 90, res_deg)
    lon_dim = np.arange(-180 + 0.5 * res_deg, 180, res_deg)
    shape = (lat_dim.size, lon_dim.size)
    lon, lat = [ar.ravel() for ar in np.meshgrid(lon_dim, lat_dim)]
    dist_coast = u_coord.dist_to_coast_nasa(lat, lon, highres=True, signed=True).reshape(shape)

    ds = xr.Dataset(
        data_vars={"dist_coast": (["lat", "lon"], dist_coast)},
        coords={"lat": ("lat", lat_dim), "lon": ("lon", lon_dim)}
    )
    u_io.write_safely(ds, path)
    return ds


def equalize_coords(ds, ds_ref):
    """Set data set's coordinate values to those of the reference data set

    If the difference between the old and new coordinate values is larger than half a grid cell, an
    Exception is raised.

    Parameters
    ----------
    ds : xr.Dataset or xr.DataArray
        A data set (or array) with dimensions called "lon" and "lat" (other dimensions remain
        unchanged).
    ds_ref : xr.Dataset or xr.DataArray
        A data set (or array) with dimensions called "lon" and "lat". This grid is expected to have
        the same shape as the grid in `ds`, and the values should only differ non-significantly.

    Returns
    -------
    xr.Dataset or xr.DataArray
    """
    for c in ["lat", "lon"]:
        res = np.abs(ds[c].values[1] - ds[c].values[0])
        assert np.abs(ds[c].values - ds_ref[c].values).max() < 0.05 * res
        ds = ds.assign_coords({c: ds_ref[c].values})
    return ds


def densify_dim(coords, coords_ref, enlarge=False):
    """Fill an irregularly spaced dimension with regularly spaced reference

    The dimension is only densified and not enlarged. The result will have the same range as the
    source dimension, but with a buffer of one reference step size added.

    Parameters
    ----------
    coords : ndarray
        The coordinate/index values of the dimension.
    coords_ref : ndarray
        The coordinate/index values of the (uniformly spaced) reference dimension. This
        dimension is expected to contain all the values contained in `coords`, but may be
        larger. It is also expected to be in ascending order.
    enlarge : bool, optional
        If True, enlarge the dimension's value range to the reference's. Default: False

    Returns
    -------
    new_coords : ndarray
        The new coordinate/index values. This array contains at least the values contained in
        the original coordinate array (and more if necessary). The new values are equally
        spaced and sorted in ascending order.
    """
    coords_uniq = np.unique(coords)
    dx = coords_ref[1] - coords_ref[0]
    if enlarge:
        new_coords = coords_ref.copy()
    else:
        new_coords = coords_ref[
            (coords_ref >= coords_uniq[0] - 1.5 * dx)
            & (coords_ref <= coords_uniq[-1] + 1.5 * dx)
        ]
    # overwrite with matching values from original coords (to avoid precision issues)
    mask = (np.abs(coords_uniq[:, None] - new_coords[None, :]) < 0.5 * dx).any(axis=0)
    new_coords[mask] = coords_uniq
    return new_coords


def densify_grid(ds, ds_ref, fill_value=0, enlarge=False):
    """Reindex an irregularly spaced lat/lon grid data set to a regularly spaced (reference) grid

    The longitudinal coordinates of both source and destination grid are normalized in a first
    step to deal with grids that cross the antimeridian.

    The grid is only densified and not enlarged. The result will have the same extent as the source
    grid, but with a buffer of one grid cell added.

    Parameters
    ----------
    ds : xr.Dataset or xr.DataArray
        A data set (or array) with dimensions called "lon" and "lat" (other dimensions remain
        unchanged).
    ds_ref : xr.Dataset or xr.DataArray
        A data set (or array) with uniformly spaced dimensions called "lon" and "lat". This grid
        is expected to contain all the grid points contained in `ds`.
    fill_value : numeric, optional
        Fill the new grid cells with this value. Default: 0
    enlarge : bool, optional
        If True, enlarge the grid's extent to the reference's. Default: False

    Returns
    -------
    ds_new : xr.Dataset
        This data set's lat/lon dimensions contain at least the values contained in the original
        data set (and more if necessary). The new coordinate values are equally spaced and
        sorted in ascending order.
    """
    mid_lon = np.mean(u_coord.lon_bounds(ds["lon"].values))
    ds = ds.assign_coords(lon=u_coord.lon_normalize(ds["lon"].values.copy(), center=mid_lon))
    ds_ref = ds_ref.assign_coords(lon=u_coord.lon_normalize(
        ds_ref["lon"].values.copy(), center=mid_lon,
    ))
    ds = (
        ds
        .reindex(lon=densify_dim(
            ds["lon"].values,
            np.unique(ds_ref["lon"].values),
            enlarge=enlarge,
        ))
        .reindex(lat=densify_dim(
            ds["lat"].values,
            np.unique(ds_ref["lat"].values),
            enlarge=enlarge,
        ))
    )
    for v in ds.data_vars:
        # don't apply fillna to variables that are not affected
        if "lon" in ds[v].dims or "lat" in ds[v].dims:
            ds[v] = ds[v].fillna(fill_value)
    return ds


def restrict_dim_to_ref(ds, ds_ref, dim, pad_steps=None, pad_val=None):
    """Restrict a data set's dimension to the value range from a reference data set

    Both data sets are required to contain a variable called `dim` and the ranges should
    at least overlap. Conceptually, the data set `ds` is expected to cover a larger range than
    the reference data set `ds_ref`.

    Parameters
    ----------
    ds : xr.Dataset or xr.DataArray
        Data set with a dimension `dim` and a coordinate variable of the same name.
    ds_ref : xr.Dataset or xr.DataArray
        Data set with a (coordinate or data) variable `dim`.
    dim : str
        Name of the dimension.
    pad_steps : float, optional
        If given, add a padding in terms of step sizes in `ds`. Default: None
    pad_val : float, optional
        If given, add an explicit padding (same units as coordinates in `ds`). Default: None

    Returns
    -------
    xr.Dataset or xr.DataArray
    """
    pad = (
        (0 if pad_val is None else pad_val)
        if pad_steps is None else
        pad_steps * np.abs(ds[dim].values[1] - ds[dim].values[0])
    )
    ref_min = ds_ref[dim].values.min()
    ref_max = ds_ref[dim].values.max()
    idx = (
        (ds[dim] <= ref_max + pad)
        & (ds[dim] >= ref_min - pad)
    ).values.nonzero()[0]
    if idx.size < 2:
        d_min, d_max = ds[dim].values.min(), ds[dim].values.max()
        raise ValueError(
            f"The dimension '{dim}' ({d_min}-{d_max}) does not cover the range of the"
            f" reference dimension ({ref_min}-{ref_max})."
        )
    with dask.config.set(**{'array.slicing.split_large_chunks': True}):
        return ds.isel(indexers={dim: slice(idx[0], idx[-1] + 1)})


def restrict_ds_to_ref(ds, ds_ref, pad_steps=None, pad_vals=None):
    """Restrict a data set's time/lat/lon extent to the extent of a reference data set

    The longitudinal coordinates are normalized, centered around the reference data set's
    longitudinal range.

    Parameters
    ----------
    ds : xr.Dataset or xr.DataArray
        Data set with dimensions "time", "lat", and "lon" (other dimensions remain unchanged).
    ds_ref : xr.Dataset or xr.DataArray
        Data set with (coordinate or data) variables "time", "lat", and "lon".
    pad_steps : float or tuple of floats, optional
        If given, add a padding in terms of step sizes in `ds`. If a tuple is given, the order is
        assumed to be "time, lat, lon". Default: None
    pad_vals : float or tuple of floats, optional
        If given, add an explicit padding (same units as coordinates in `ds`). If a tuple is
        given, the order is assumed to be "time, lat, lon". Default: None

    Returns
    -------
    xr.Dataset or xr.DataArray
    """
    pad_steps = (
        (pad_steps,) * 3 if pad_steps is None or np.isscalar(pad_steps) else tuple(pad_steps)
    )

    pad_vals = tuple(0 if v is None else v for v in (
        (pad_vals,) * 3 if pad_vals is None or np.isscalar(pad_vals) else pad_vals
    ))

    ds = restrict_dim_to_ref(ds, ds_ref, "time", pad_steps=pad_steps[0], pad_val=pad_vals[0])
    ds = restrict_dim_to_ref(ds, ds_ref, "lat", pad_steps=pad_steps[1], pad_val=pad_vals[1])

    # Load the data into memory before restricting in the lon dimension because otherwise dask
    # might decide that it's necessary to load a whole file (chunk) of the full grid data
    # into memory.
    ds.load()

    mid_lon_ref = 0.5 * (ds_ref["lon"].values.min() + ds_ref["lon"].values.max())
    ds = ds.assign_coords(lon=u_coord.lon_normalize(
        ds["lon"].values.copy(), center=mid_lon_ref,
    ))
    ds = ds.reindex(lon=np.unique(ds["lon"].values))
    ds = restrict_dim_to_ref(ds, ds_ref, "lon", pad_steps=pad_steps[2], pad_val=pad_vals[2])
    return ds


def transform_from_ds(ds):
    """Determine the affine transformation describing a data set's lat/lon grid

    Parameters
    ----------
    ds : xr.Dataset or xr.DataArray
        Data set with dimensions "lat" and "lon". The grid is assumed to be regularly spaced.

    Returns
    -------
    rasterio.Affine
    """
    lon, lat = ds["lon"].values, ds["lat"].values
    dlon, dlat = lon[1] - lon[0], lat[1] - lat[0]
    return rasterio.Affine(
        dlon, 0, lon[0] - 0.5 * dlon,
        0, dlat, lat[0] - 0.5 * dlat,
    )


def reproject_da_to_ref(da, da_ref, resampling="nearest"):
    """Reproject a gridded DataArray to the grid of a reference array

    Note that the array is assumed to have a "time" dimension.

    Parameters
    ----------
    da : xr.DataArray
        Array with dimensions "time", "lat" and "lon" (in that order!).
    da_ref : xr.Dataset or xr.DataArray
        Array with dimensions "lat" and "lon" (in any order).
    resampling : str or rasterio.warp.Resampling, optional
        The grid interpolation method to use. Default: "nearest"

    Returns
    -------
    xr.DataArray
    """
    if isinstance(resampling, str):
        resampling = rasterio.warp.Resampling[resampling]
    n_bands = da.sizes["time"]
    dst_shape = (da_ref.sizes["lat"], da_ref.sizes["lon"])
    src_transform = transform_from_ds(da)
    dst_transform = transform_from_ds(da_ref)
    dst_data = np.zeros((n_bands,) + dst_shape)
    rasterio.warp.reproject(
        source=da.values,
        destination=dst_data,
        src_transform=src_transform,
        src_crs=u_const.EPSG_4326,
        dst_transform=dst_transform,
        dst_crs=u_const.EPSG_4326,
        resampling=resampling,
    )
    dst_da = (
        da.copy()
        .reindex(
            lat=da_ref["lat"].values,
            lon=da_ref["lon"].values,
        )
    )
    dst_da.values[:] = dst_data
    return dst_da


def centroids_to_grid(centr, subset=None):
    """Convert Centroids object to a (non-regular) grid

    Parameters
    ----------
    centr : Centroids
        The centroids to convert. Only the lat and lon attributes are used.
    subset : ndarray, optional
        If given, only the subset of the centroids defined by this integer
        array (of indices) is considered. Default: None

    Returns
    -------
    grid_lat : ndarray
        Latitudinal coordinates of the grid.
    grid_lon : ndarray
        Longitudinal coordinates of the grid.
    grid_inv : ndarray
        Inverse mapping from the (flat) centroids to a
        flattened version of the grid:

        >>> grid_data_flat = grid_data.ravel()
        >>> grid_data_flat[grid_inv] = centr_data

        If `subset` is given, this only maps to data on the subset.
    """
    lat, lon = centr.lat, centr.lon
    if subset is not None:
        lat, lon = lat[subset], lon[subset]
    grid_lat, glat_inv = np.unique(lat, return_inverse=True)
    grid_lon, glon_inv = np.unique(lon, return_inverse=True)
    grid_shape = (grid_lat.shape[0], grid_lon.shape[0])
    grid_inv = np.ravel_multi_index((glat_inv, glon_inv), dims=grid_shape)
    return grid_lat, grid_lon, grid_inv


def unfilter_centroids(mask_centr_filter, inten, field, vectorial=False):
    """Sparse arrays on full centroids from sparse arrays on filtered centroids

    Parameters
    ----------
    mask_centr_filter : ndarray
        Subselection of centroids.
    inten : csr_matrix
        Sparse matrix with a single row, and one column per (filtered!) centroid.
    field : csr_matrix
        Rows correspond to time steps, columns are for (filtered!) centroids (and the
        two vectorial components, raveled, if applicable).
    vectorial : bool, optional
        If True, assume that the data is vectorial (two components). Default: False

    Returns
    -------
    inten : csr_matrix
        Sparse matrix with a single row, and one column per centroid.
    field : csr_matrix
        Rows correspond to time steps, columns are for centroids (and the
        two vectorial components, raveled, if applicable).
    """
    n_centroids = mask_centr_filter.size
    [idx_centr_filter] = mask_centr_filter.nonzero()
    n_centr_filter = idx_centr_filter.size
    n_positions = field.shape[0]

    assert inten.shape == (1, n_centr_filter)
    assert field.shape == (n_positions, n_centr_filter * (2 if vectorial else 1))

    inten = scipy.sparse.csr_matrix(
        (inten.data, idx_centr_filter[inten.indices], inten.indptr),
        shape=(1, n_centroids),
    )

    if vectorial:
        fidx_centr_filter = np.zeros((n_centr_filter, 2), dtype=np.int64)
        fidx_centr_filter[:, 0] = 2 * idx_centr_filter[None]
        fidx_centr_filter[:, 1] = 2 * idx_centr_filter[None] + 1
        fidx_centr_filter = fidx_centr_filter.ravel()
        field_shape = (n_positions, n_centroids * 2)
    else:
        fidx_centr_filter = idx_centr_filter
        field_shape = (n_positions, n_centroids)
    field = scipy.sparse.csr_matrix(
        (field.data, fidx_centr_filter[field.indices], field.indptr),
        shape=field_shape,
    )

    return inten, field


def sparse_field_to_ds(varname, field, time, centr, inten=None, vectorial=False):
    """Convert a sparse time-dependent field to an xarray Dataset

    Parameters
    ----------
    varname : str
        The name of the main data variable in the output dataset.
    field : csr_matrix
        Rows correspond to time steps, columns are for centroids (and the
        two vectorial components, raveled, if applicable).
    time : ndarray
        Time stamp of each storm step.
    centr : Centroids
        The centroids on which the rain field is defined.
    inten : csr_matrix, optional
        If given, use this as the maximum along the time dimension. This is used to restrict the
        output grid to the area affected by the field. Default: None
    vectorial : bool, optional
        If True, assume that the data is vectorial (two components). Default: False

    Returns
    -------
    xr.Dataset
    """
    n_time = time.size
    n_centroids = centr.size
    n_vec = 2 if vectorial else 1
    if inten is None:
        inten = np.linalg.norm(
            field
            .toarray()
            .reshape(n_time, n_centroids, n_vec),
            axis=-1,
        ).max(axis=0)
        inten = scipy.sparse.csr_matrix(inten)
    inten.eliminate_zeros()
    aff_indices = inten.indices
    n_aff = aff_indices.size

    grid_lat, grid_lon, grid_inv = centroids_to_grid(centr, subset=aff_indices)
    grid_shape = (grid_lat.size, grid_lon.size)
    grid_size = np.prod(grid_shape)

    aff_mask_vec = np.zeros((n_centroids, n_vec), dtype=bool)
    aff_mask_vec[aff_indices, :] = 1
    aff_mask_vec = aff_mask_vec.ravel()
    aff_indices_vec = aff_mask_vec.nonzero()[0]
    field = (
        field[:, aff_indices_vec]
        .toarray()
        .reshape(n_time, n_aff, n_vec)
    )

    intensities = np.zeros((n_time, grid_size))
    intensities[:, grid_inv] = np.linalg.norm(field, axis=-1)
    intensities = intensities.reshape((n_time,) + grid_shape)

    ds = xr.Dataset(data_vars={
        varname: (["step", "lat", "lon"], intensities),
        "time": ("step", time),
    }, coords={
        "lat": ("lat", grid_lat),
        "lon": ("lon", grid_lon),
        "step": ("step", np.arange(n_time)),
    })

    for v in ["lat", "lon", "time", varname]:
        if v in u_const.NC_ATTR:
            ds[v].attrs = u_const.NC_ATTR[v]

    return ds
