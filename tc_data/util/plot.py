
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import cartopy.io.shapereader as shpreader
import climada.util.coordinates as u_coord
import matplotlib.cm
import matplotlib.colors as mcolors
import matplotlib.gridspec as mgridspec
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import numpy as np


CAT_COLORS = ['#5ebaff', '#00faf4', '#ffffcc', '#ffe775', '#ffc140', '#ff8f20', '#ff6060']
CAT_NAMES = {
    "wind": ['TD', 'TS', '1',  '2', '3', '4', '5'],
    "rain": ['0', '1',  '2', '3', '4', '5'],
}
CAT_THRESH = {
    "wind": [0, 18, 33, 43, 50, 58, 70],  # Saffir-Simpson
    "rain": [0, 100, 263, 426, 589, 750],  # TC severity scale by Bloemendaal et al.
}


class LinearSegmentedNormalize(mcolors.Normalize):
    """Piecewise linear color normalization."""
    def __init__(self, vthresh):
        """Initialize normalization

        Parameters
        ----------
        vthresh : list
            Equally distributed to the interval [0,1].
        """
        self.vthresh = vthresh
        self.values = np.linspace(0, 1, len(self.vthresh))
        mcolors.Normalize.__init__(self, vmin=vthresh[0], vmax=vthresh[-1], clip=False)

    def __call__(self, value, clip=None):
        return np.ma.masked_array(np.interp(value, self.vthresh, self.values))


def colormap_severity(key):
    cmap_tc = mcolors.LinearSegmentedColormap.from_list(
        "tc_severity", CAT_COLORS[1:]
    )
    norm_tc = LinearSegmentedNormalize(CAT_THRESH[key][1:])
    return cmap_tc, norm_tc


def plot_field(ax, da, inten_thresh, cmap, norm, proj_data):
    im = da.values
    psize = (
        abs(da["lon"].values[1] - da["lon"].values[0]),
        abs(da["lat"].values[1] - da["lat"].values[0]),
    )
    bounds = (
        da["lon"].values.min() - 0.5 * psize[0],
        da["lat"].values.min() - 0.5 * psize[1],
        da["lon"].values.max() + 0.5 * psize[0],
        da["lat"].values.max() + 0.5 * psize[1],
    )
    extent = (bounds[0], bounds[2], bounds[1], bounds[3])
    im = ax.imshow(
        im, cmap=cmap, norm=norm, alpha=(im > inten_thresh).astype(float),
        extent=extent, origin="lower", interpolation="none", rasterized=True,
        transform=proj_data,
    )
    ax.set_extent((
        extent[0] - 0.5 * psize[0], extent[1] + 0.5 * psize[0],
        extent[2] - 0.5 * psize[1], extent[3] + 0.5 * psize[1],
    ), crs=proj_data)
    im.set_clip_box(ax.bbox)


def add_arrows(line, offset=0, skip=2, step=2, lbd=0.1, **kwargs):
    color = line.get_color()
    xdata = line.get_xdata()
    ydata = line.get_ydata()
    for start_ind in range(offset, len(xdata) - step, skip):
        end_ind = start_ind + step
        xend, yend = xdata[end_ind], ydata[end_ind]
        xstart = lbd * xdata[start_ind] + (1 - lbd) * xend
        ystart = lbd * ydata[start_ind] + (1 - lbd) * yend
        ann = line.axes.annotate('',
            xytext=(xstart, ystart), xy=(xend, yend),
            arrowprops=dict(arrowstyle="->", color=color),
            **kwargs,
        )
        ann.arrow_patch.set_clip_box(line.axes.bbox)


def plot_track_w_inten(gs, haz_type, tr, da, title):
    cmap, norm = colormap_severity(haz_type)

    mid_lon = np.mean(u_coord.lon_bounds(tr["lon"].values))
    proj_data = ccrs.PlateCarree()
    proj_plot = ccrs.PlateCarree(central_longitude=mid_lon)

    fig = plt.gcf()
    ax = fig.add_subplot(gs, projection=proj_plot)

    ax.add_feature(
        cfeature.OCEAN.with_scale("10m"),
        facecolor='#f0f0f0',
        edgecolor="none",
        lw=0.5,
        zorder=0,
    )

    [line] = ax.plot(
        tr["lon"].values, tr["lat"].values, transform=proj_data,
        lw=0.5, color="k", zorder=10,
    )

    # one arrow every 24 hours with 14 hour offset
    res_time_min = (tr["time"].values[1] - tr["time"].values[0]) / np.timedelta64(1, "m")
    hour_steps = 60 / res_time_min
    offset = int(9 * hour_steps)
    skip = int(24 * hour_steps)
    add_arrows(line, offset=offset, skip=skip, transform=proj_data, zorder=10)

    plot_field(ax, da, CAT_THRESH[haz_type][1], cmap, norm, proj_data)

    if title is not None:
        ax.text(0.01, 0.99, title, va="top", transform=ax.transAxes)

    ax.patch.set_alpha(0)

    xmin, xmax = [x + mid_lon for x in ax.get_xlim()]
    ymin, ymax = ax.get_ylim()
    width, height = xmax - xmin, ymax - ymin
    dx = 10.0
    xlocs = np.arange(np.ceil(xmin / dx) * dx, np.ceil(xmax / dx) * dx, dx)
    ylocs = np.arange(np.ceil(ymin / dx) * dx, np.ceil(ymax / dx) * dx, dx)
    xlocs = xlocs[xlocs - xmin > 0.02 * width]
    ylocs = ylocs[ymax - ylocs > 0.05 * height]
    ax.gridlines(ylocs=ylocs, xlocs=xlocs, crs=proj_data, linewidth=0.5, linestyle="--")
    for loc in xlocs:
        ax.text(
            loc, ymin + 0.012 * height,
            f"{abs(loc):.0f}°{'W' if loc < 0 else 'E'}",
            rotation="vertical", ha="right", va="bottom",
            fontsize=8, color="grey",
            transform=proj_data,
        )
    for loc in ylocs:
        ax.text(
            xmax - 0.005 * width, loc,
            f"{abs(loc):.0f}°{'S' if loc < 0 else 'N'}",
            ha="right", va="bottom",
            fontsize=8, color="grey",
            transform=proj_data,
        )

    cax = ax.inset_axes([0.5, 0.82, 0.4, 0.05])
    cbar = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=cmap), cax=cax, orientation="horizontal")
    cbar.set_ticks(norm.values)
    cbar.set_ticklabels([f"{v:.0f} ({CAT_NAMES[haz_type][i + 1]})" for i, v in enumerate(norm.vthresh)])
    cbar.set_label(
        "1-minute sustained wind speed in m/s\nwith corresponding Saffir-Simpson category in parenthesis"
        if haz_type == "wind" else
        "Accumulated rainfall in mm with\nTropical Cyclone Severity Scale in parenthesis"
    )

    ax_legend = ax.inset_axes([0.5, 0.62, 0.4, 0.05], frameon=False)
    N = 32
    [line] = ax_legend.plot(np.linspace(0, 1, N), np.full(N, 0), lw=1, color='k')
    add_arrows(line, offset=3, skip=6, step=1)
    ax_legend.set_xlabel("Storm track with direction marking arrows at 24-hourly intervals")
    ax_legend.set_xticks([])
    ax_legend.set_yticks([])

    return ax, ax_legend
