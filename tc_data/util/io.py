
from datetime import datetime as dt
import hashlib
import pathlib
import signal
import sys
from typing import Union
import uuid

from climada.hazard.tc_tracks import LOGGER, TCTracks, set_category
import git
import numpy as np
import pandas as pd
import xarray as xr

import tc_data
import tc_data.util.constants as u_const


def read_isimip3a_as_climada_tracks(path, year=None):
    """Read the TC tracks from the ISIMIP3a NetCDF file as a CLIMADA TCTracks object

    Parameters
    ----------
    path : Path or str
        Path to the NetCDF file containing historical TC tracks, distributed as part of ISIMIP3a.
    year : int, optional
        If given, only load track data from this year.

    Returns
    -------
    TCTracks
    """
    ds_combined = (
        xr.open_dataset(path)
        .rename_vars({
            "sid": "sid",
            "time": "time",
            "lat": "lat",
            "lon": "lon",
            "basin": "basin",
            "pres": "central_pressure",
            "penv": "environmental_pressure",
            "windspatialmax": "max_sustained_wind",
            "nature": "nature",
            "rmw": "radius_max_wind",
            "roci": "radius_oci",
            "u850": "u850",
            "v850": "v850",
            "t600": "t600",
        })
    )

    if year is not None:
        ds_combined = ds_combined.sel(storm=(ds_combined["time"].dt.year == year).any(dim="step"))

    # when writing '<U*' and reading in again, xarray reads as dtype 'object'. undo this:
    for varname in ds_combined.data_vars:
        if ds_combined[varname].dtype == "object":
            ds_combined[varname] = ds_combined[varname].fillna("").astype(str)

    ds_combined["id_no"] = (
        ds_combined["sid"]
        .str.replace('N', '0')
        .str.replace('S', '1')
        .astype(float)
    )

    data = []
    for i in range(ds_combined.sizes["storm"]):
        # extract a single storm and restrict to valid time steps
        track = (
            ds_combined
            .isel(storm=i)
            .dropna(dim="step", how="any", subset=["time", "lat", "lon"])
        )
        # convert the "time" variable to a coordinate
        track = track.drop_vars(["storm", "step"]).rename(step="time")
        track = track.assign_coords(time=track["time"]).compute()
        # convert 0-dimensional variables to attributes:
        attr_vars = [v for v in track.data_vars if track[v].ndim == 0]
        track = (
            track
            .assign_attrs({v: track[v].item() for v in attr_vars})
            .drop_vars(attr_vars)
        )
        track.attrs['orig_event_flag'] = True
        track.attrs['max_sustained_wind_unit'] = "kn"
        track.attrs['central_pressure_unit'] = "mb"
        track.attrs['category'] = set_category(track["max_sustained_wind"].values, 'kn')
        data.append(track)
    return TCTracks(data)


class GracefulInterruptHandler():
    """Context manager for handling SIGTERM (e.g. when running in `standby` QOS on SLURM)

    Example
    -------
    >>> with GracefulInterruptHandler() as h:
    >>>     # do something that may not be interrupted ...
    >>>     if h.interrupted:
    >>>         print("Quitting gracefully after SIGTERM ...")
    >>>         sys.exit()
    >>> # if there was no interruption, the code continues here
    """
    def __init__(self, sigs=[signal.SIGTERM, signal.SIGINT]):
        self.sigs = sigs

    def __enter__(self):
        self.interrupted = False
        self.released = False

        self.original_handlers = []
        for sig in self.sigs:
            self.original_handlers.append(signal.getsignal(sig))
            signal.signal(sig, self.handle)

        return self

    def __exit__(self, type, value, tb):
        self.release()

    def handle(self, signum, frame):
        print(f"[{dt.now()}] Signal ({signum}) received. Cleaning up ...")
        self.release()
        self.interrupted = True

    def release(self):
        if self.released:
            return False

        for sig, handler in zip(self.sigs, self.original_handlers):
            signal.signal(sig, handler)

        self.released = True
        return True


def path_exists_safely(path, delete=True):
    """Check if file exists and is not corrupt

    For data that has been written using `write_safely`, this will check whether the file and
    its checksum file exist and whether the checksum matches. If not, the data is considered as
    corrupted and the file and its checksum file are removed (if they exist).

    An empty file (size 0) is considered to be non-existent, even if the checksum file matches.

    Parameters
    ----------
    path : Path
        The path to the file to check.
    delete : bool, optional
        If False, do not remove the file and its checksum file in case of corruption.
        Default: True

    Returns
    -------
    bool
    """
    checksum_path = path.parent / f"{path.name}.sha256sum"
    if not path.exists() or path.stat().st_size == 0:
        exists = False
    else:
        exists = check_sha256sum(path)
    if delete and not exists:
        path.unlink(missing_ok=True)
        checksum_path.unlink(missing_ok=True)
    return exists


def _get_package_version():
    """Determine this package's version string

    If the code is in a git repository, the commit hash is used (with prefix git@).

    Returns
    -------
    str
    """
    repo_path = pathlib.Path(tc_data.__file__).parent.parent
    try:
        repo = git.Repo(repo_path)
        version = f"git@{repo.head.commit.hexsha}"
    except git.exc.InvalidGitRepositoryError:
        version = tc_data.__version__
    return version


def _prepare_dataset_for_writing(ds, encoding):
    """Prepare dataset for writing to NetCDF

    Ensures that data is compressed (zlib=True), properly chunked, and that some general meta data
    such as the code version or the missing value is stored in global and data variable attributes.
    Attributes that are already set in `encoding` are not overwritten.

    Parameters
    ----------
    ds : xr.Dataset
        The dataset to store in a NetCDF file.
    encoding : dict or None
        Directives to pass on to the `to_netcdf` function, overwriting internal defaults.

    Returns
    -------
    ds : xr.Dataset
        The dataset with global attributes set to defaults.
    encoding : dict
        Directives to pass on to the `to_netcdf` function.
    """
    ds = ds.compute()
    encoding = {} if encoding is None else encoding

    for v in ds.variables:
        v_encoding = {}

        # chunk and compress multi-dimensional variables
        if ds[v].ndim > 1:
            # Do not compress string/object type variables.
            #
            # Background: Prior to libnetcdf version 4.9, the zlib=True setting was ignored
            # for variable-length data types (like string). With version 4.9, an error was
            # raised instead. This behavior was effectively reverted in this PR:
            # https://github.com/Unidata/netcdf-c/pull/2716
            #
            # Since we want this code to work for libnetcdf 4.9, we do not set zlib=True for
            # string-type variables.
            #
            if ds[v].dtype.kind not in ["U", "O"]:
                v_encoding['zlib'] = True

            t_dims = ["storm", "step", "time", "iso"]
            all_t_dims = all(d in t_dims for d in ds[v].dims)
            v_encoding["chunksizes"] = tuple(
                1 if d in t_dims and not all_t_dims else ds[v].sizes[d]
                for d in ds[v].dims
            )

        if ds[v].dtype.kind == "f":
            v_encoding['_FillValue'] = None
            if v not in ds[v].coords:
                v_encoding['missing_value'] = u_const.NC_MISSING_VALUE
            ds[v] = ds[v].astype(np.float64)

        if ds[v].dtype.kind == "M":
            v_encoding = {**v_encoding, **u_const.NC_TIME_ENCODING}

        encoding[v] = {**v_encoding, **encoding.get(v, {})}

    ds.attrs = {
        **u_const.NC_ATTR_GLOBAL,
        "code_version": _get_package_version(),
        "isimip_id": str(uuid.uuid4()),
        **ds.attrs,
    }

    return ds, encoding


class _TCTracks(TCTracks):
    def write_hdf5(self, file_name, complevel=5):
        """Write TC tracks in NetCDF4-compliant HDF5 format.

        Fix: Do not compress string/object type variables to be compatible with libnetcdf 4.9.

        Parameters
        ----------
        file_name: str or Path
            Path to a new HDF5 file. If it exists already, the file is overwritten.
        complevel : int
            Specifies a compression level (0-9) for the zlib compression of the data.
            A value of 0 or None disables compression. Default: 5
        """
        data = []
        for track in self.data:
            # convert "time" into a data variable and use a neutral name for the steps
            track = track.rename(time="step")
            track["time"] = ("step", track["step"].values)
            track["step"] = np.arange(track.sizes["step"])
            # change dtype from bool to int to be NetCDF4-compliant
            track.attrs['orig_event_flag'] = int(track.attrs['orig_event_flag'])
            data.append(track)

        # concatenate all data sets along new dimension "storm"
        ds_combined = xr.combine_nested(data, concat_dim=["storm"])
        ds_combined["storm"] = np.arange(ds_combined.sizes["storm"])

        # convert attributes to data variables of combined dataset
        df_attrs = pd.DataFrame([t.attrs for t in data], index=ds_combined["storm"].to_series())
        ds_combined = xr.merge([ds_combined, df_attrs.to_xarray()])

        encoding = {
            v: dict(zlib=True, complevel=complevel)
            for v in ds_combined.variables
            if ds_combined[v].dtype.kind not in ["U", "O"]
        }
        LOGGER.info('Writing %d tracks to %s', self.size, file_name)
        ds_combined.to_netcdf(file_name, encoding=encoding)


def write_safely(obj, path, unlink=None, encoding=None):
    """Write NetCDF/CSV/TCTracks data together with checksum file

    If you later find that the file exists, but the checksum file doesn't exist or doesn't match,
    then the data is probably corrupted, e.g. because the process was killed during writing.

    Parameters
    ----------
    obj : pd.DataFrame or xr.Dataset or TCTracks
        The data to store in the given location. A DataFrame is written as CSV,
        a Dataset is written as NetCDF, and a TCTracks object is written as HDF5.
    path : Path
        The path to the destination file.
    unlink : list of Path, optional
        If given, remove these (temporary) files after the data has been
        successfully written to the specified location.
    encoding : dict, optional
        In case of xr.Dataset, pass on to the `to_netcdf` function. If not specified otherwise,
        all data variables are compressed by default (zlib=True). Default: None

    See also
    --------
    path_exists_safely
    """
    with GracefulInterruptHandler() as h:
        print(f"Writing to {path.name} ... ", end="")
        path.parent.mkdir(parents=True, exist_ok=True)
        if isinstance(obj, pd.DataFrame):
            obj.to_csv(path, index=None)
        elif isinstance(obj, xr.Dataset):
            obj, encoding = _prepare_dataset_for_writing(obj, encoding)
            obj.to_netcdf(path, format="NETCDF4", engine="netcdf4", encoding=encoding)
        elif isinstance(obj, TCTracks):
            print("")
            _TCTracks(data=obj.data).write_hdf5(path)
        else:
            NotImplementedError(f"Object type not supported: {type(obj)}")
        write_sha256sum(path)
        print("done.")

        if unlink is not None:
            print(f"Deleting temporary files ... ", end="")
            for p in unlink:
                p.unlink(missing_ok=True)
                (p.parent / f"{p.name}.sha256sum").unlink(missing_ok=True)
            print("done.")

        if h.interrupted:
            print("Quitting gracefully after SIGTERM ...")
            sys.exit()


def check_sha256sum(path: Union[pathlib.Path, str]) -> bool:
    """Compare the SHA256 checksum to that stored in the accompanying *.sha256sum file

    Returns False, if the *.sha256sum does not exist or contains a reference to a different path.

    Parameters
    ----------
    path : Path or str
        Path to file.

    Returns
    -------
    bool
    """
    checksum_file = path.parent / f"{path.name}.sha256sum"
    if not checksum_file.exists():
        return False
    checksum_data = checksum_file.read_text().strip().split()
    if len(checksum_data[0]) != 64:
        return False
    if len(checksum_data) >= 2 and pathlib.Path(checksum_data[1]).name != path.name:
        return False
    checksum = compute_sha256sum(path)
    return checksum == checksum_data[0]


def write_sha256sum(path: Union[pathlib.Path, str]) -> None:
    """Write the SHA256 checksum to a *.sha256sum file

    Parameters
    ----------
    path : Path or str
        Path to file.
    """
    checksum_file = path.parent / f"{path.name}.sha256sum"
    checksum = compute_sha256sum(path)
    checksum_file.write_text(f"{checksum} {path.name}")


def compute_sha256sum(path: Union[pathlib.Path, str]) -> str:
    """Compute the SHA256 checksum of a file

    Parameters
    ----------
    path : Path or str
        Path to file.

    Returns
    -------
    str
    """
    h = hashlib.sha256()
    chunk_num_blocks = 128
    chunk_size = chunk_num_blocks * h.block_size
    with open(path, "rb") as fp:
        while chunk := fp.read(chunk_size):
            h.update(chunk)
    return h.hexdigest()
