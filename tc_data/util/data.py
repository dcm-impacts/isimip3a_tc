
from climada.hazard import Centroids, TCTracks
import numpy as np
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.grid as u_grid
import tc_data.util.io as u_io


def hyde_centroids():
    """Centroids at 300as (5 arc-minuts or 1/12 degree) resolution

    The centroids agree with the cell centers of the grid used in HYDEv3.3

    Returns
    -------
    centroids : Centroids
        CLIMADA object with lat/lon coordinate points.
    dist_coast : ndarray
        One value per coordinate point for (signed) distance to the coastline (in meters).
    """
    path = u_const.CACHE_DIR / "centroids_hyde.hdf5"
    if path.exists():
        print(f"Load cache data from {path} ...")
        cen = Centroids.from_hdf5(str(path))
    else:
        ds_grid = u_grid.regular_grid(u_const.HYDE_RES_AS)
        lat_dim, lon_dim = ds_grid["lat"].values, ds_grid["lon"].values
        # double-check that this agrees with the Hyde grid
        assert lat_dim.size == 2160
        assert lon_dim.size == 4320
        lon, lat = [ar.ravel() for ar in np.meshgrid(lon_dim, lat_dim)]
        cen = Centroids(
            lat=lat, lon=lon, dist_coast=ds_grid["dist_coast"].values.copy().ravel(),
        )
        cen.write_hdf5(str(path))

    # new CLIMADA versions replaced the `dist_coast` attribute with a `get_dist_coast` function
    # and moved the (optional) dist_coast data to a column of the internal GeoDataFrame
    if hasattr(cen, "dist_coast"):
        dist_coast = cen.dist_coast
        cen.dist_coast = np.abs(dist_coast)
    else:
        dist_coast = cen.gdf["dist_coast"].values
        cen.gdf["dist_coast"] = np.abs(cen.gdf["dist_coast"])

    return cen, dist_coast


def country_masks(res_am):
    """Load reference annual national population data for a single year

    It is made sure that the grid coordinates agree with the HYDE population grid data.

    Parameters
    ----------
    res_am : int
        Resolution of the raster in arc-minutes.

    Returns
    -------
    da_land_sea_mask : xr.DataArray
        Array with dimensions "lon" and "lat". Inland grid cells get a value of 1, ocean grid cells
        get a value 0, and coastal grid cells get a fractional value according to the fraction of
        the cell that is covered by land.
    da_masks : xr.DataArray
        Array with dimensions "iso", "lon", and "lat".
    """
    suffix = "_5arcmin" if res_am == 5 else ""
    cache_path = u_const.CACHE_DIR / f"countrymasks_fractional{suffix}.nc"
    if u_io.path_exists_safely(cache_path):
        da_masks = xr.open_dataset(cache_path)["fractional_mask"]
    else:
        da_masks = (
            xr.open_dataset(u_const.POP_INPUT_DIR / f"countrymasks_fractional{suffix}.nc")
            .to_array(dim="iso", name="fractional_mask")
            # reverse order of latitudinal coords to agree with Hyde input
            .sel(lat=slice(None, None, -1))
        )
        da_masks = da_masks.assign_coords(iso=da_masks["iso"].str.lstrip("m_"))
        da_masks = u_grid.equalize_coords(da_masks, u_grid.regular_grid(res_am * 60))
        u_io.write_safely(da_masks.to_dataset(), cache_path)

    # reduce to actual country masks (exclude world and region masks, and ignored countries)
    da_land_sea_mask = da_masks.sel(iso="world")
    da_masks = da_masks.sel(iso=(da_masks["iso"].str.len() == 3))

    return da_land_sea_mask, da_masks


def country_exposures(res_am, exposure_spec):
    """Get the specified assets on a per-country basis

    Parameters
    ----------
    res_am : int
        Resolution of the raster in arc-minutes.
    exposure_spec : str
        One of "area", "popYYYY", or "assetsYYYY", where "YYYY" is a year.

    Returns
    -------
    xr.DataArray
    """
    suffix = "_5arcmin" if res_am == 5 else ""
    cache_path = u_const.CACHE_DIR / "exposures" / f"country_{exposure_spec}{suffix}.nc"
    exposure_type = "area" if exposure_spec == "area" else exposure_spec[:-4]
    exposure_year = None if exposure_spec == "area" else int(exposure_spec[3:])
    if u_io.path_exists_safely(cache_path):
        da = xr.open_dataset(cache_path)[exposure_type]
    else:
        da_land_sea_mask, da_masks = country_masks(res_am)
        if exposure_type == "area":
            area_cell_sqkm = (u_const.ONE_LAT_KM * res_am / 60.0)**2
            da = area_cell_sqkm * np.cos(np.radians(da_masks["lat"]))
        elif exposure_type == "pop":
            da = xr.open_dataset(
                u_const.OUTPUT_DIR
                / f"pop_{res_am:d}arcmin"
                / f"popc_{exposure_year}.nc"
            )["total-population"].fillna(0.0)
        else:
            # load the GDP layer multiplied by asset conversion factors
            raise NotImplementedError
        da = da * da_masks / np.fmax(np.spacing(1), da_land_sea_mask)
        da.name = exposure_type
        u_io.write_safely(da.to_dataset(), cache_path)
    return da


def pop_hyde_unscaled(res_am, layer, year):
    """Load HYDE population layer at specified resolution for a single year

    This loads the raw (unscaled) HYDE data.

    Parameters
    ----------
    res_am : int
        Resolution of the raster in arc-minutes.
    layer : str
        One of "pop", "urb", or "rur".
    year : int
        The year for which to extract data.

    Returns
    -------
    xr.DataArray with dimensions "lon" and "lat"
    """
    suffix = "_5arcmin" if res_am == 5 else ""
    path = u_const.POP_INPUT_DIR / f"hyde_v3.3_{layer}c_1850_2021{suffix}.nc"
    da_pop = xr.open_dataset(path)
    msk_yr = (da_pop["time"].dt.year == year)
    # constantly extend to 2021 if needed
    if msk_yr.sum() == 0 and year == 2021:
        msk_yr = (da_pop["time"].dt.year == 2020)
    da_pop = da_pop.sel(time=msk_yr).squeeze()["pop"]

    # override the coordinates using the reference grid
    da_pop = u_grid.equalize_coords(da_pop, u_grid.regular_grid(res_am * 60))

    return da_pop


def ibtracs_by_year(year, time_step_h=None):
    """Load TC tracks from IBTrACS for a single year

    Parameters
    ----------
    year : int
        The year for which to extract TC tracks.
    time_step_h : float, optional
        If given, interpolate the track data to this resolution (in hours). Default: None

    Returns
    -------
    TCTracks
    """
    if time_step_h is not None:
        path_h = u_const.CACHE_DIR / f"tracks_hist_{time_step_h}h" / f"{year}.nc"
        if u_io.path_exists_safely(path_h):
            print(f"Load cache data from {path_h} ...")
            return TCTracks.from_hdf5(path_h)

    path = u_const.OUTPUT_DIR / "tracks_hist" / f"{year}.nc"
    if u_io.path_exists_safely(path):
        print(f"Load track data from {path} ...")
        tr = TCTracks.from_hdf5(path)
    else:
        tr = TCTracks.from_ibtracs_netcdf(
            year_range=(year, year),
            additional_variables=["nature"],
            estimate_missing=True,
            interpolate_missing=True,
        )
        u_io.write_safely(tr, path)

    if time_step_h is not None:
        tr.equal_timestep(time_step_h=time_step_h)
        u_io.write_safely(tr, path_h)

    return tr


def dense_wind(storm_id, model, chunk_size=None):
    """Load a wind data set on a dense (300 arc-second) grid

    Parameters
    ----------
    storm_id : str
        The IBTrACS storm ID of the event.
    model : str
        Load the wind field data that uses this TC wind model.
    chunk_size : int, optional
        If given, return the data in chunks (over the time dimension). In that case, the function
        returns an iterator. Default: None

    Returns
    -------
    ds_dense : xr.Dataset
        The wind data on a dense (300 arc-second) grid.
    ds : xr.Dataset
        The wind data on the original (sparse) grid.
    """
    return_iterator = chunk_size is not None
    path = (
        u_const.OUTPUT_DIR / f"wind_{model.lower()}"
        / f"{model.lower()}_obsclim_historical_wind_{storm_id}.nc"
    )
    print(f"Reading from {path} ...")
    with xr.open_dataset(path) as ds:
        ds = ds.compute().copy()
    chunk_size = ds.sizes["step"] if chunk_size is None else chunk_size

    ds_grid = None
    if 0 not in ds["wind"].shape:
        ds_grid = u_grid.regular_grid(u_const.HYDE_RES_AS)

    it = _dense_wind_in_chunks(ds, ds_grid, chunk_size)
    return it if return_iterator else next(it)


def _dense_wind_in_chunks(ds, ds_grid, chunk_size):
    """Load a wind data set on the given grid in chunks along the "step" dimension

    Parameters
    ----------
    ds : xr.Dataset
        The full wind data on the original (sparse) grid.
    ds_grid : xr.Dataset
        A data set (or array) with uniformly spaced dimensions called "lon" and "lat". This grid
        is expected to contain all the grid points contained in `ds`.
    chunk_size : int
        The size of each chunk along the "step" dimension.

    Returns
    -------
    Iterator with values as returned by `dense_wind`
    """
    for offset in range(0, ds.sizes["step"], chunk_size):
        ds_chunk = ds.isel(step=slice(offset, offset + chunk_size))
        ds_chunk_reg = None
        if 0 not in ds_chunk["wind"].shape:
            ds_chunk_reg = u_grid.densify_grid(ds_chunk, ds_grid)
        yield ds_chunk_reg, ds_chunk


def dense_rain_era5(storm_id):
    """Load an ERA5 rain data set on a dense (300 arc-second) grid

    Parameters
    ----------
    storm_id : str
        The IBTrACS storm ID of the event.

    Returns
    -------
    ds_dense : xr.Dataset
        The rain data on a dense (300 arc-second) grid.
    ds : xr.Dataset
        The rain data on the original (sparse) grid.
    """
    path = u_const.OUTPUT_DIR / "rain_era5" / f"{storm_id}.nc"
    print(f"Reading from {path} ...")
    with xr.open_dataset(path) as ds:
        ds = ds.compute().copy()
    ds_reg = None
    if 0 not in ds["rain"].shape:
        ds_grid = u_grid.regular_grid(u_const.HYDE_RES_AS)
        ds_reg = u_grid.densify_grid(ds, ds_grid)
    return ds_reg, ds
