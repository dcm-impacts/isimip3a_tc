
import pathlib

from climada.hazard.trop_cyclone import KN_TO_MS
from climada.util.constants import ONE_LAT_KM


BASE_DIR = pathlib.Path(__file__).resolve().parent.parent.parent
"""General location of the input, output or cache data."""

CACHE_DIR = BASE_DIR / "cache"
"""General location of cache data."""

INPUT_DIR = BASE_DIR / "input"
"""General location of input data."""

POP_INPUT_DIR = BASE_DIR / "input" / "pop"
"""Location of gridded (Hyde), and national (ISIMIP3a) population time series."""

OUTPUT_DIR = BASE_DIR / "output"
"""General location of output data."""

SRTM_ELEVATION_TIF = INPUT_DIR / "srtm15_v2.3_300as.tif"
"""Topography (land surface elevation) raster data

Upscaled to 300 arc-seconds from original SRTM15+ data (tiled, 'index.vrt') using "gdalwarp":

$ gdalwarp index.vrt -tr 0.083333333333333 -0.083333333333333 -r average \
    -co COMPRESS=DEFLATE srtm15_v2.3_300as.tif
"""

ISIMIP3A_HIST_PERIOD = (1950, 2021)
"""We will provide the TC data in ISIMIP3a for this range of year (extension of TCE-DAT)"""

RECENT_HIST_PERIOD = (1980, 2019)
"""Some of the data is only produced for this (more reliable) period"""

KM_TO_M = 1000
"""Conversion factor for conversion from km to m"""

DEG_TO_AS = 3600
"""Conversion factor for conversion from degrees to arc-seconds"""

EPSG_4326 = "epsg:4326"
"""EPSG ID of the lat/lon projection"""

HYDE_RES_AS = 300
"""Resolution (in arc-seconds) of the gridded HYDE population data"""

MINRAIN_MM = 1
"""We truncate the rain field at 1 mm (over the whole storm life time)."""

MINWIND_MS = 2
"""We truncate the storm size at 1500 km (min. winds of 2 m/s), following this study:

    Pérez-Alarcón et al. (2021): Comparative climatology of outer tropical cyclone size
    using radial wind profiles. Weather and Climate Extremes 33: 100366.
    https://dx.doi.org/10.1016/j.wace.2021.100366
"""

MAX_STORM_SIZE_KM = 1500
"""We truncate the storm size at 1500 km (min. winds of 2 m/s), see `MINWIND_MS`."""

NC_TIME_ENCODING = {
    "units": "hours since 1901-01-01 00:00",
    "calendar": "proleptic_gregorian",
}
"""Default time encoding in ISIMIP3a NetCDF files"""

NC_MISSING_VALUE = 1e20
"""Default missing value in ISIMIP3a NetCDF files"""

NC_ATTR = {
    "sid": {
        "standard_name": "sid",
        "long_name": "IBTrACS Serial ID",
    },
    "lat": {
        "standard_name": "latitude",
        "long_name": "Latitude",
        "units": "degrees_north",
        "axis": "Y",
    },
    "lon": {
        "standard_name": "longitude",
        "long_name": "Longitude",
        "units": "degrees_east",
        "axis": "X",
    },
    "time": {
        "standard_name": "time",
        "long_name": "Time",
        "axis": "T",
    },
    "wind": {
        "standard_name": "wind",
        "long_name": "1-minute sustained wind speed",
        "units": "m s-1",
    },
    "windspatialmax": {
        "standard_name": "windspatialmax",
        "long_name": "Maximum 1-minute sustained wind speed",
        "units": "knots",
    },
    "windlifetimemax": {
        "standard_name": "windlifetimemax",
        "long_name": "Lifetime maximum 1-minute sustained wind speed",
        "units": "m s-1",
    },
    "rain": {
        "standard_name": "rain",
        "long_name": "Total rainfall",
        "units": "mm",
    },
    "max_rain": {
        "standard_name": "max_rain",
        "long_name": "Maximum 24-hourly rainfall total during the whole storm duration",
        "units": "mm",
    },
}
"""Default variable attributes in ISIMIP3a NetCDF files"""

NC_ATTR_GLOBAL = {
    "reference": "https://doi.org/10.48364/ISIMIP.605192",
    "references": "The dataset is described in Frieler et al. 2024 <https://doi.org/10.5194/gmd-17-1-2024>.",
    "institution": "Potsdam Institute for Climate Impact Research (PIK)",
    "project": "Inter-Sectoral Impact Model Intercomparison Project phase 3a (ISIMIP3a)",
    "contact": ", ".join([
        "Thomas Vogt <thomas.vogt@pik-potsdam.de>",
        "Matthias Mengel <matthias.mengel@pik-potsdam.de>",
    ]),
    "code": "https://gitlab.pik-potsdam.de/isimip3a-tc-data/isimip3a_tc",
}
"""Default global attributes in ISIMIP3a NetCDF files"""
