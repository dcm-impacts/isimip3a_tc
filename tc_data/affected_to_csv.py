
import sys

import numpy as np
import pandas as pd
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.io as u_io


def merge_affected(model, soc):
    """Merge all NetCDF files with affected exposures into a single data frame

    The result is stored in a CSV file.

    Parameters
    ----------
    model : str
        The wind field model to use.
    soc : str
        One of "hist", or "2015".
    """
    out_path = (
        u_const.OUTPUT_DIR / "affected" / "csv"
        / f"{model.lower()}_obsclim_{soc}soc_affected-by-year-and-country.csv"
    )

    if u_io.path_exists_safely(out_path):
        return

    period = u_const.ISIMIP3A_HIST_PERIOD
    l_years = np.arange(period[0], period[1] + 1)

    dfs = []
    in_path = u_const.OUTPUT_DIR / "affected"
    for year in l_years:
        df = xr.open_mfdataset([
            in_path / f"{model.lower()}_obsclim_{soc}soc_affected{exposure}_{year}.nc"
            for exposure in ["area", "pop"]  # TODO: include assets
        ], combine="by_coords").to_dataframe()
        df[df == 0] = np.nan
        dfs.append(
            df.dropna(how="all").fillna(0.0)
        )
    df = (
        pd.concat(dfs)
        .reset_index()
        .rename(columns={
            "storm": "storm_id",
            "iso": "country",
        })
    )
    u_io.write_safely(df, out_path)


def main():
    model = "H08"
    if "ER11" in sys.argv[1:]:
        model = "ER11"
    soc = "hist"
    if "2015soc" in sys.argv[1:]:
        soc = "2015"
    merge_affected(model, soc)


if __name__ == "__main__":
    main()
