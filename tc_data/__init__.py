
import warnings

warnings.filterwarnings(
    "ignore",
    message="Index.ravel returning ndarray is deprecated; in a future version",
    module="xarray",
    category=FutureWarning,
)

warnings.filterwarnings(
    "ignore",
    message="invalid value encountered in cast",
    module="xarray",
    category=RuntimeWarning,
)
