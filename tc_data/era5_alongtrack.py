
import os
import sys

from climada.hazard import TCTracks
import climada.util.coordinates as u_coord
from climada.util.constants import ONE_LAT_KM
import numpy as np
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.grid as u_grid
import tc_data.util.io as u_io


TRACKS_DIR = u_const.OUTPUT_DIR / "tracks_hist"


def interp1d_linear_weights_from_gridspec(x, x0, dx, nx):
    """Weights realising the linear interpolation in a regular grid at given points

    Parameters
    ----------
    x : ndarray
        Points at which to interpolate the regular grid.
    x0 : float
        The origin of the grid (cell center of the first grid cell).
    dx : float
        The step size of the grid.
    nx : int
        The number of grid cells.

    Returns
    -------
    weights : ndarray of shape (npoints, nx)
        Each row contains the grid weights for a point.
    """
    pos = ((x - x0) / dx)[:, None]
    idx = np.floor(pos).astype(int)
    offset = pos - idx
    weights = np.zeros((x.size, nx))
    np.put_along_axis(weights, idx, 1 - offset, axis=1)
    np.put_along_axis(weights, idx + 1, offset, axis=1)
    return weights


def interp1d_linear_weights_from_gridarr(x, x_grid):
    """Weights realising the linear interpolation in a regular grid at given points

    Convenience wrapper around `interp1d_linear_weights_from_gridspec` for cases where the grid
    is given as an array of cell centers.

    Parameters
    ----------
    x : ndarray
        Points at which to interpolate the regular grid.
    x_grid : ndarray
        The cell centers of the grid. Note that the step sizes are assumed to be uniform.

    Returns
    -------
    weights : ndarray of shape (npoints, ngrid)
        Each row contains the grid weights for a point.
    """
    return interp1d_linear_weights_from_gridspec(
        x, x_grid[0], x_grid[1] - x_grid[0], x_grid.size
    )


def central_value_along_track(da, track, t_res="3h", t_interp_method="linear"):
    """Interpolated values of the time-dependent raster data at track points

    Parameters
    ----------
    da : xr.DataArray
        Time-dependent raster data to interpolate along the track.
        An array with dimensions "time", "lat", and "lon".
    track : xr.Dataset
        Track specification with values for "lat" and "lon" along a dimension "time".
    t_res : str, optional
        Resample frequency. If not None, resample the raster data to this temporal resolution
        using averaging before interpolating to the track time steps. This is useful if the
        temporal resolution of the raster data is higher (e.g. hourly) than the track's temporal
        resolution (e.g. 3-hourly). Default: "3h"
    t_interp_method : str, optional
        The method to use for the time interpolation of the raster data to the time steps of the
        track. For supported methods, see `xr.Dataset.interp`. Default: "linear"

    Returns
    -------
    result : xr.DataArray
        An array with dimension "time", same size as time dimension of `track`.
    """
    track["lon"].values[:] = u_coord.lon_normalize(track["lon"].values.copy(), center=None)
    da = u_grid.restrict_ds_to_ref(da, track, pad_steps=(3, 2, 2))
    if t_res is not None:
        da = da.resample(time=t_res).mean()
    da = da.interp(time=track["time"].values, method=t_interp_method)
    ds = da.to_dataset()
    for c in ["lat", "lon"]:
        ds[f"{c}_weights"] = (
            ["time", c], interp1d_linear_weights_from_gridarr(track[c].values, ds[c].values),
        )
    res = (da * ds["lat_weights"] * ds["lon_weights"]).sum(dim=["lat", "lon"])
    res.name = da.name
    res.attrs = da.attrs
    return res


def annular_mean_along_track(
    da, track, r_inner_km, r_outer_km, t_res="3h", t_interp_method="linear",
):
    """Take averages of time-dependent raster data over ring-shaped (annular) areas around track

    Parameters
    ----------
    da : xr.DataArray
        Time-dependent raster data to average along the track.
        An array with dimensions "time", "lat", and "lon".
    track : xr.Dataset
        Track specification with values for "lat" and "lon" along a dimension "time".
    r_inner_km : float
        Radius (in km) of inner circle defining the annular region.
    r_outer_km : float
        Radius (in km) of outer circle defining the annular region.
    t_res : str, optional
        Resample frequency. If not None, resample the raster data to this temporal resolution
        using averaging before interpolating to the track time steps. This is useful if the
        temporal resolution of the raster data is higher (e.g. hourly) than the track's temporal
        resolution (e.g. 3-hourly). Default: "3h"
    t_interp_method : str, optional
        The method to use for the time interpolation of the raster data to the time steps of the
        track. For supported methods, see `xr.Dataset.interp`. Default: "linear"

    Returns
    -------
    result : xr.DataArray
        An array with dimension "time", same size as time dimension of `track`.
    """
    track["lon"].values[:] = u_coord.lon_normalize(track["lon"].values.copy(), center=None)
    r_outer_deg = r_outer_km / (
        ONE_LAT_KM * np.cos(np.radians(np.abs(track["lat"].values).max()))
    )
    da = u_grid.restrict_ds_to_ref(
        da, track,
        pad_steps=(3, None, None),
        pad_vals=(None, r_outer_deg, r_outer_deg),
    )
    if t_res is not None:
        da = da.resample(time=t_res).mean()
    da = da.interp(time=track["time"].values, method=t_interp_method)

    lons, lats = [ar.ravel() for ar in np.meshgrid(da["lon"].values, da["lat"].values)]
    dists_km = u_coord.dist_approx(
        track["lat"].values[None], track["lon"].values[None],
        lats[None], lons[None],
    )[0].reshape(da.shape)

    ds = da.to_dataset()
    ds["weights"] = (["lat", "lon"], (
        np.cos(np.radians(np.abs(lats))).reshape(da.shape[1:])
    ))
    ds["mask"] = (["time", "lat", "lon"], (
        (dists_km >= r_inner_km) & (dists_km <= r_outer_km)
    ))
    da_weights = ds["mask"] * ds["weights"]
    da_weights /= da_weights.sum(dim=["lat", "lon"])

    res = (da_weights * da).sum(dim=["lat", "lon"])
    res.name = da.name
    res.attrs = da.attrs

    return res


def get_era5_dataset(variable, year):
    """Hourly ERA5 dataset for the given variable and year

    The dataset also includes months belonging to the two neighboring years if available. The
    longitudinal (latitudinal) dimension is called "lon" ("lat"). The time dimension is called
    "time".

    Parameters
    ----------
    variable : str
        Short name of the ERA5 variable. Example: "t600"
    year : int
        Year to be covered by the dataset.

    Returns
    -------
    xr.Dataset
    """
    period = (year - 1, year + 1)
    path = u_const.INPUT_DIR / f"era5_{variable}"
    files = [
        list(path.glob(f"*_ERA5_{year:04d}{month:02d}_hourly.nc"))
        for year in range(period[0], period[1] + 1)
        for month in range(1, 13)
    ]
    assert all(len(f) <= 1 for f in files)
    files = [f[0] for f in files if len(f) > 0]
    d_rename = {"longitude": "lon", "latitude": "lat"}
    return xr.open_mfdataset(files).rename(d_rename)


def extract_era5_along_tracks(year, enforce=False):
    """Extract additional track variables from ERA5

    Following Lu et al. 2018, the following variables are extracted:
    - u850, v850 : wind speeds on 850 hPa level in 200-500 km annulus
    - t600 : temperature on 600 hPa level at center

    The values are stored in place (in the annual trackset NetCDF files)

    Parameters
    ----------
    year : int
        The year for which to process tracks.
    enforce : bool, optional
        If True, enforce data extraction even if the variables are already present. Default: False
    """
    path = TRACKS_DIR / f"{year}.nc"
    tracks = TCTracks.from_hdf5(path)

    variables = ["t600", "u850", "v850"]
    if not enforce:
        if all(v in tr.variables for tr in tracks.data for v in variables):
            if all(np.isfinite(tr[v].values).any() for tr in tracks.data for v in variables):
                print(f"All variables are present already, skipping {year} ...")
                return

    era5_ds = {v: get_era5_dataset(v, year) for v in variables}
    for track in tracks.data:
        print(track.attrs["sid"])
        track["t600"] = central_value_along_track(era5_ds["t600"]["t"], track)
        for v in ["u", "v"]:
            track[f"{v}850"] = annular_mean_along_track(
                era5_ds[f"{v}850"][v], track, 200, 500,
            )

    u_io.write_safely(tracks, path)


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    period = u_const.ISIMIP3A_HIST_PERIOD

    # 72 configurations
    year = np.arange(period[0], period[1] + 1)[task]
    extract_era5_along_tracks(year)


if __name__ == "__main__":
    main()
