
import os
import sys

import numpy as np
import pandas as pd
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.data as u_data
import tc_data.util.io as u_io


def count_affected(model, exposure, soc, year):
    """Count affected exposure values for the given year

    The result is stored in a CSV file.

    Parameters
    ----------
    model : str
        The wind field model to use.
    exposure : str
        One of "area", "pop", or "assets".
    soc : str
        One of "hist", or "2015".
    year : int
        The year to process.
    """
    out_path = (
        u_const.OUTPUT_DIR / "affected"
        / f"{model.lower()}_obsclim_{soc}soc_affected{exposure}_{year}.nc"
    )

    if u_io.path_exists_safely(out_path):
        return

    res_am = int(u_const.HYDE_RES_AS / 60)
    exposure_spec = (
        exposure
        if exposure == "area" else
        f"{exposure}{year if soc == 'hist' else soc}"
    )
    da_assets = u_data.country_exposures(res_am, exposure_spec)
    wind_dir = u_const.OUTPUT_DIR / f"wind_{model.lower()}" / "max_wind"
    wind_file = wind_dir / f"{model.lower()}_obsclim_historical_windlifetimemax_{year}.nc"
    da_wind = xr.open_dataset(wind_file)["windlifetimemax"]

    l_aff_ctry = []
    affected_storm_idx = np.arange(da_wind.sizes["storm"])
    affected_ctry_idx = np.arange(da_assets.sizes["iso"])
    for thresh_kn in [34, 48, 64, 96]:
        print(f"Compute affected {exposure} for {thresh_kn}kn threshold ...")
        aff_values = xr.dot(
            da_wind.isel(storm=affected_storm_idx) >= thresh_kn * u_const.KN_TO_MS,
            da_assets.isel(iso=affected_ctry_idx),
            dims=["lat", "lon"],
        )
        aff_values.name = "value"
        aff_mask = (aff_values > 0)
        affected_storm_idx = affected_storm_idx[aff_mask.any(dim="iso").values]
        affected_ctry_idx = affected_ctry_idx[aff_mask.any(dim="storm").values]
        if 0 in [affected_storm_idx.size, affected_ctry_idx.size]:
            print("No countries or storms are affected for this threshold!")
            break
        aff_values = (
            aff_values
            .to_series()
            .iloc[aff_mask.values.ravel()]
            .reset_index()
        )
        aff_values["year"] = year
        aff_values["thresh_kn"] = thresh_kn
        aff_values["exposure"] = exposure
        l_aff_ctry.append(aff_values)

    df = (
        pd.concat(l_aff_ctry)
        .set_index(["year", "iso", "storm", "thresh_kn", "exposure"])["value"]
        .unstack(level=["thresh_kn", "exposure"])
    )
    df.columns = [f"{t}kn{e}" for t, e in df.columns]
    ds = df.to_xarray().fillna(0.0)
    u_io.write_safely(ds, out_path)


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    model = "H08"
    if "ER11" in sys.argv[1:]:
        model = "ER11"

    exposure = "area"
    if "pop" in sys.argv[1:]:
        exposure = "pop"
    elif "assets" in sys.argv[1:]:
        exposure = "assets"

    soc = "hist"
    if "2015soc" in sys.argv[1:]:
        soc = "2015"

    period = u_const.ISIMIP3A_HIST_PERIOD
    l_years = np.arange(period[0], period[1] + 1)

    # 72 configurations
    year = l_years[task]
    count_affected(model, exposure, soc, year)


if __name__ == "__main__":
    main()
