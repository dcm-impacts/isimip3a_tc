
import os
import sys

import climada.util.coordinates as u_coord
import numpy as np
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.io as u_io
import tc_data.util.grid as u_grid


def aggregate_by_storm(variable, year, model, era5=False):
    """Aggregate the variable per storm and store as one file per year

    Parameters
    ----------
    variable : str
        One of "rain" or "wind".
    year : int
        The year to process.
    model : str
        The wind field model to use.
    era5 : bool, optional
        If True, use ERA5 rain fields. Default: False
    """
    input_dir = u_const.OUTPUT_DIR / (
        f"{variable}_era5_{model.lower()}"
        if era5 else
        f"{variable}_{model.lower()}"
    )

    maxvariable = f"{variable}lifetimemax" if variable == "wind" else f"max_{variable}"

    outpath = input_dir / f"max_{variable}" / (
        # variable names are not allowed to contain underscores in the file name
        f"{model.lower()}_obsclim_historical_{maxvariable.replace('_', '')}_{year}.nc"
    )
    if u_io.path_exists_safely(outpath):
        print(f"Data for model {model} and year {year} already exists, nothing to do. Exiting ...")
        return

    input_files = sorted(
        input_dir.glob(f"{model.lower()}_obsclim_historical_{variable}_{year}*.nc")
    )
    if len(input_files) == 0:
        script_name = f"compute_{variable}{'s' if variable == 'wind' else ''}"
        raise FileNotFoundError(
            f"No {variable} data found for year {year} in {input_dir}."
            f" Make sure to run `{script_name}` (for year {year}) before aggregating."
        )
    print(f"Aggregating {len(input_files)} storms for year {year} ...")

    l_ds = []
    for p in input_files:
        ibtracs_id = p.stem.split('_')[-1]
        print(f"Aggregating {ibtracs_id} ...")
        with xr.open_dataset(p) as ds:
            ds = ds.drop_vars(["time"])
            win_size = min(ds.sizes["step"], 24)
            l_ds.append(
                (
                    ds if variable == "wind" else
                    ds.rolling(step=win_size).sum()
                )
                .max(dim="step")
                .compute()
                .expand_dims({"storm": [ibtracs_id]})
                .rename_vars({variable: maxvariable})
            )

    print(f"Merge into a single global data set ...")
    ds = xr.combine_nested(l_ds, concat_dim="storm")

    ds_grid = u_grid.regular_grid(u_const.HYDE_RES_AS)
    ds = u_grid.densify_grid(ds, ds_grid, enlarge=True)

    lons = u_coord.lon_normalize(ds["lon"].values.copy())
    ds["lon"].values[:] = lons
    ds = ds.reindex(lon=np.unique(lons))
    ds = u_grid.equalize_coords(ds, ds_grid)

    # Sometimes the writing fails with OOM error unless we reset the encoding here:
    ds = ds.reset_encoding()

    ds[maxvariable].attrs = u_const.NC_ATTR[maxvariable]
    ds["storm"].attrs = u_const.NC_ATTR["sid"]
    ds["lat"].attrs = u_const.NC_ATTR["lat"]
    ds["lon"].attrs = u_const.NC_ATTR["lon"]

    u_io.write_safely(ds, outpath)


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    model = "H08"
    if "ER11" in sys.argv[1:]:
        model = "ER11"

    era5_fields = False
    variable = "wind"
    if "rain" in sys.argv[1:]:
        variable = "rain"
    elif "era5" in sys.argv[1:]:
        variable = "rain"
        era5_fields = True

    period = u_const.ISIMIP3A_HIST_PERIOD
    l_years = np.arange(period[0], period[1] + 1)

    # 72 configurations
    year = l_years[task]
    aggregate_by_storm(variable, year, model, era5=era5_fields)


if __name__ == "__main__":
    main()
