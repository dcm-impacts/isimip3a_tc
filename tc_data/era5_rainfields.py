
import os
import sys

import climada.util.coordinates as u_coord
import numpy as np
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.data as u_data
import tc_data.util.grid as u_grid
import tc_data.util.io as u_io


PERIOD = u_const.ISIMIP3A_HIST_PERIOD

# In ERA5-Land, precipitation is cumulative, so that the value at 15:00 is the total rain from 00:00
# until 15:00 (00:00 is for the accumulated total from the previous day).
ERA5L_PATH = u_const.INPUT_DIR /  "era5l_pr"
ERA5L_FNAME = (
    "total_precipitation_ERA5-Land_"
    "{year:04d}{month:02d}*-{year:04d}{month:02d}*.nc"
)
ERA5L_FILES = [
    list(ERA5L_PATH.glob(ERA5L_FNAME.format(year=year, month=month)))[0]
    for year in range(PERIOD[0], PERIOD[1] + 1)
    for month in range(1, 13)
]

# In ERA5 (global), precipitation is accumulated from the previous hour, so that the value at 15:00
# is the total rain from 14:00 until 15:00 (00:00 is for 23:00 of the previous day until 00:00)
ERA5_PATH = u_const.INPUT_DIR /  "era5_pr"
ERA5_FNAME = (
    "total_precipitation_ERA5_{year:04d}{month:02d}_hourly.nc"
)
ERA5_FILES = [
    ERA5_PATH / ERA5_FNAME.format(year=year, month=month)
    for year in range(PERIOD[0], PERIOD[1] + 1)
    for month in range(1, 13)
]


def load_era5_rain(ds_ref):
    """Load hourly ERA5 precipitation data on grid/times from a reference data set

    ERA5-Land data is used over land, and global ERA5 data everywhere else. The data is
    reprojected to the reference data set's grid using nearest-neighbor interpolation.

    Parameters
    ----------
    ds_ref : xr.Dataset or xr.DataArray
        Data set with (coordinate or data) variables "time", "lat" and "lon". The lat/lon grid is
        expected to be equally spaced.

    Returns
    -------
    xr.Dataset
    """
    d_rename = {"longitude": "lon", "latitude": "lat"}
    da = {
        "era5": (ERA5_FILES, "tp"),
        "era5l": (ERA5L_FILES, "tp"),
    }
    for key, (files, v) in da.items():
        with xr.open_mfdataset(files) as _ds:
            _da = _ds.rename(d_rename)[v]
            _da = u_grid.restrict_ds_to_ref(_da, ds_ref, pad_steps=1).compute().copy()
        # convert from m to mm
        _da *= 1000
        if key == "era5l":
            # convert from cumulative to hourly rain
            da_diff = _da.diff(dim="time")
            # reset entries at 01:00 to their original values
            idx_diff = (da_diff["time"].dt.hour == 1).values.nonzero()[0]
            da_diff.values[idx_diff, :, :] = (
                _da.values[idx_diff + 1, :, :]
            )
            _da = da_diff
        _da = _da.reindex(time=ds_ref["time"].values)
        _da = u_grid.reproject_da_to_ref(_da, ds_ref)
        da[key] = _da
    da = da["era5l"].fillna(da["era5"]).rename({"time": "step"})
    da.name = "rain"
    ds = da.to_dataset()
    ds["step"] = ds_ref["step"]
    ds["time"] = ds_ref["time"]
    return ds


def extract_era5_rainfields(year, model):
    """Extract (and store) ERA5 rain data for all storms in a given year

    The rain data is only stored in grid cells with non-zero wind speeds. The resulting NetCDF
    file has exactly the same shape as the wind field data.

    Parameters
    ----------
    year : int
        The year for which to process storms.
    model : str
        Use the wind field data that uses this TC wind model.
    """
    wind_dir = u_const.OUTPUT_DIR / f"wind_{model.lower()}"
    rain_dir = u_const.OUTPUT_DIR / f"rain_era5_{model.lower()}"
    paths = sorted(wind_dir.glob(f"{model.lower()}_obsclim_historical_wind_{year}*.nc"))
    for path in paths:
        sid = path.stem.split("_")[-1]
        outpath = rain_dir / f"{model.lower()}_obsclim_historical_rain_{sid}.nc"
        if u_io.path_exists_safely(outpath):
            continue
        l_ds_rain = []
        for ds_wind_reg, ds_wind in u_data.dense_wind(sid, model, chunk_size=100):
            if ds_wind_reg is None or ds_wind["time"].dt.year.values[-1] > PERIOD[1]:
                # this storm does not affect any coastal areas
                # or part of it is outside of the historical period
                ds_rain = (
                    ds_wind[["step", "time", "wind"]]
                    .rename({"wind": "rain"})
                    .copy(deep=True)
                )
                ds_rain["rain"] *= 0.0
            else:
                ds_rain = load_era5_rain(ds_wind_reg)
                ds_rain["rain"] *= (ds_wind_reg["wind"] > 0).astype(int)
                # reindex to the original (irregularly spaced) grid with lon range [-180,180]
                ds_rain = ds_rain.assign_coords(
                    lon=u_coord.lon_normalize(ds_rain["lon"].values.copy(), center=0.0)
                )
                assert np.isin(ds_wind["lon"].values, ds_rain["lon"].values).all()
                assert np.isin(ds_wind["lat"].values, ds_rain["lat"].values).all()
                ds_rain = ds_rain.reindex({c: ds_wind[c].values for c in ["lon", "lat"]})
            l_ds_rain.append(ds_rain)
        ds = xr.concat(l_ds_rain, "step")

        ds["rain"].attrs = u_const.NC_ATTR["rain"]

        u_io.write_safely(ds, outpath)


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    model = "H08"
    if "ER11" in sys.argv[1:]:
        model = "ER11"

    # 72 configurations
    year = np.arange(PERIOD[0], PERIOD[1] + 1)[task]
    extract_era5_rainfields(year, model)


if __name__ == "__main__":
    main()
