
import os
import sys

from climada.hazard import TCTracks
from climada_petals.hazard import TCRain
import numpy as np
import scipy.sparse
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.data as u_data
import tc_data.util.grid as u_grid
import tc_data.util.io as u_io


OUTPUT_FILE = lambda model, sid: (
    u_const.OUTPUT_DIR / f"rain_{model.lower()}"
    / f"{model.lower()}_obsclim_historical_rain_{sid}.nc"
)

def _hourly_rain_from_half_hourly_rates(track, rainrates):
    """Convert 0.5-hourly rain rates to hourly rain totals

    Parameters
    ----------
    track : xr.Dataset
        A single TC track data set.
    rainrates : csr_matrix
        0.5-hourly rain rates corresponding to the given track.
        Rows correspond to time steps, columns are for centroids.

    Returns
    -------
    times : ndarray
        Time stamp of each hourly step.
    field : csr_matrix
        Rows correspond to time steps, columns are for centroids.
    """
    # convert the rain rates (in mm/h) to rain totals (in mm)
    np.testing.assert_array_equal(track["time_step"].values, 0.5)
    field = rainrates * 0.5

    # convert 0.5 hourly to hourly resolution

    # extend at beginning and end if necessary
    if track["time"].dt.minute.values[0] == 0:
        times = track["time"].values[::2]
        field_ext = [field[:1, :], field]
    else:
        times = track["time"].values[1::2]
        field_ext = [field]
    if track["time"].dt.minute.values[-1] == 0:
        field_ext.append(field[-1:, :])
    field = scipy.sparse.vstack(field_ext, format="csr")

    # sum over time ranges (field_m : 00:15-01:15) and (field_p : 00:45-01:45)
    field_m, field_p = [
        scipy.sparse.vstack([
            scipy.sparse.csr_matrix(field[i:i + 2, :].sum(axis=0))
            for i in range(offset, field.shape[0] - 1, 2)
        ], format="csr")
        for offset in [0, 1]
    ]

    # average of sums over backward and forward time range
    field = 0.5 * (field_m + field_p)

    return times, field


def compute_rain_for_tracks(tracks, centroids, mask_centr_filter, model, pool):
    """Compute (and store) the rain fields for a set of TC tracks

    Parameters
    ----------
    tracks : TCTracks
        A set of TC tracks.
    centroids : Centroids
        Centroids where to model the TC winds.
    mask_centr_filter : ndarray
        Subselection of centroids to use.
    model : str
        Parametric wind field model to use.
    pool : pathos.pool
        Pool that will be used for parallel computation of wind fields.
    """
    haz = TCRain.from_tracks(
        tracks,
        centroids=centroids.select(sel_cen=mask_centr_filter),
        model="TCR",
        model_kwargs=dict(
            wind_model=model,
            matlab_ref_mode=False,
            elevation_tif=u_const.SRTM_ELEVATION_TIF,
        ),
        store_rainrates=True,
        intensity_thres=u_const.MINRAIN_MM,
        max_latitude=89.999,
        max_dist_eye_km=u_const.MAX_STORM_SIZE_KM,
        max_dist_inland_km=1e5,
        metric="geosphere",
        max_memory_gb=10,
        pool=pool,
    )
    for track, inten, rainr in zip(tracks.data, haz.intensity, haz.rainrates):
        times, field = _hourly_rain_from_half_hourly_rates(track, rainr)
        path = OUTPUT_FILE(model, track.attrs['sid'])
        inten, field = u_grid.unfilter_centroids(mask_centr_filter, inten, field, vectorial=False)
        ds = u_grid.sparse_field_to_ds(
            "rain", field, times, centroids, inten=inten, vectorial=False,
        )
        u_io.write_safely(ds, path)


def compute_rain(year, model):
    """Compute (and store) the rain fields for all IBTrACS entries of a year

    Parameters
    ----------
    year : int
        Year for which to extract IBTrACS entries.
    model : str
        Parametric wind field model to use.
    """
    # define a process pool (optional, for execution on a SLURM cluster)
    pool = None
    if 'OMP_NUM_THREADS' in os.environ and int(os.environ['OMP_NUM_THREADS']) > 1:
        from pathos.pools import ProcessPool as Pool
        # we reserve four cores per thread, since we need at least 16 GB memory per thread
        num_threads = int(os.environ['OMP_NUM_THREADS']) // 4
        print(f"Running with {num_threads} threads.")
        pool = Pool(nodes=num_threads)

    print(f"Processing TCs for year {year}.")
    tr = u_data.ibtracs_by_year(year, time_step_h=0.5)
    tr.data = [
        t for t in tr.data
        if not u_io.path_exists_safely(OUTPUT_FILE(model, t.attrs['sid']))
    ]
    if len(tr.data) == 0:
        print("Nothing to do, all rainfield files exist already.")
        return

    cen, dist_coast = u_data.hyde_centroids()
    # ignore pixels that are more than 50 km off shore
    mask_centr_filter = (dist_coast <= 50 * u_const.KM_TO_M)

    chunksize = 4
    for chunk_start in range(0, tr.size, chunksize):
        chunk = tr.data[chunk_start:chunk_start + chunksize]
        compute_rain_for_tracks(TCTracks(data=chunk), cen, mask_centr_filter, model, pool)


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    model = "H08"
    if "ER11" in sys.argv[1:]:
        model = "ER11"

    period = u_const.ISIMIP3A_HIST_PERIOD

    # 72 configurations
    year = np.arange(period[0], period[1] + 1)[task]
    compute_rain(year, model)


if __name__ == "__main__":
    main()
