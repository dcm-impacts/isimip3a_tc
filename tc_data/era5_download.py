
import subprocess
from multiprocessing import Pool

import cdsapi

import tc_data.util.constants as u_const


def compile_cds_request(vlong, pres_level, year, month):
    req = {
        'product_type': 'reanalysis',
        'format': 'netcdf',
        'variable': vlong,
        'year': f"{year:04d}",
        'month': f"{month:02d}",
        'day': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08', '09',
            '10', '11', '12',
            '13', '14', '15',
            '16', '17', '18',
            '19', '20', '21',
            '22', '23', '24',
            '25', '26', '27',
            '28', '29', '30',
            '31',
        ],
        'time': [
            '00:00', '01:00', '02:00',
            '03:00', '04:00', '05:00',
            '06:00', '07:00', '08:00',
            '09:00', '10:00', '11:00',
            '12:00', '13:00', '14:00',
            '15:00', '16:00', '17:00',
            '18:00', '19:00', '20:00',
            '21:00', '22:00', '23:00',
        ],
    }
    if pres_level is not None:
        req['pressure_level'] = f"{pres_level}"
    return req


def compile_era5_fname(vlong, pres_level, year, month):
    varname = f"{vlong}" if pres_level is None else f"{vlong}_{pres_level}"
    return f"{varname}_ERA5_{year:04d}{month:02d}_hourly.nc"


def retrieve_compress_and_rm(cds_name, cds_request, outpath):
    tmppath = outpath.parent / (outpath.name + "4")

    c = cdsapi.Client()
    c.retrieve(cds_name, cds_request, outpath)

    proc = subprocess.Popen(
        ["cdo", "-f", "nc4", "-z", "zip", "-copy", outpath, tmppath]
    )
    proc.wait()

    outpath.unlink()
    tmppath.rename(outpath)


def main():
    period = u_const.ISIMIP3A_HIST_PERIOD
    variables = {
        't': ('temperature', 600),
        'u': ('u_component_of_wind', 850),
        'v': ('v_component_of_wind', 850),
        'pr': ('total_precipitation', None),
    }

    args = []
    for vshort, (vlong, pres_level) in variables.items():
        cds_name = f"reanalysis-era5-{'single' if pres_level is None else 'pressure'}-levels"
        outdir = u_const.INPUT_DIR / f"era5_{vshort}{'' if pres_level is None else pres_level}"
        outdir.mkdir(parents=True, exist_ok=True)
        for year in range(period[0], period[1] + 1):
            for month in range(1, 13):
                outpath = outdir / compile_era5_fname(vlong, pres_level, year, month)
                if outpath.exists():
                    continue
                print(outpath.name)
                cds_request = compile_cds_request(vlong, pres_level, year, month)
                args.append((cds_name, cds_request, outpath))

    with Pool(processes=5) as pool:
        pool.starmap(retrieve_compress_and_rm, args)


if __name__ == "__main__":
    main()
