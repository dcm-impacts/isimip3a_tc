
import os
import sys

import numpy as np
import pandas as pd
import xarray as xr

import tc_data.util.constants as u_const
import tc_data.util.data as u_data
import tc_data.util.io as u_io


PATH_POP_REF = u_const.POP_INPUT_DIR / "population_histsoc_national_annual_1950_2021.csv"
"""Annual national population counts, used as a reference for rescaling"""


def load_pop_ref(year):
    """Load reference annual national population data for a single year

    Parameters
    ----------
    year : int
        The year for which to extract data.

    Returns
    -------
    xr.DataArray with dimension "iso"
    """
    # convert from millions to absolute numbers of people
    da_pop_ref = 1e6 * (
        pd.read_csv(PATH_POP_REF, skiprows=4)
        .set_index("iso")
        .to_xarray()[str(year)]
    )
    return da_pop_ref


def rescale_single_country(da_land_sea_mask, da_masks, da_pop, da_pop_ref, scale_map, iso):
    """Rescale the given country's population according to the reference data

    The rescaling factors are written to `scale_map` in place.

    Parameters
    ----------
    da_land_sea_mask, da_masks : xr.DataArray
        Output of `u_data.load_country_masks`.
    da_pop : xr.DataArray
        Output of `u_data.pop_hyde_unscaled`.
    da_pop_ref : xr.DataArray
        Output of `load_pop_ref`.
    scale_map : xr.DataArray
        Array with dimensions "lon" and "lat", corresponding to the grid in `da_pop`.
    iso : str
        3-letter identifier of the country to rescale.
    """
    pop_ref = da_pop_ref.sel(iso=iso).item()
    da_mask = da_masks.sel(iso=iso)
    msk_nz = (da_mask.values > 0)
    msk_nan = np.isnan(scale_map.values)
    msk = msk_nz & ~msk_nan
    unscalable = (
        scale_map.values[msk] * da_pop.values[msk] * da_mask.values[msk]
        / da_land_sea_mask.values[msk]
    ).astype(np.float64).sum()

    scalable = 0
    scale_factor = 1.0
    msk = msk_nz & msk_nan
    if msk.sum() > 0:
        scalable = (
            da_pop.values[msk] * da_mask.values[msk]
            / da_land_sea_mask.values[msk]
        ).astype(np.float64).sum()
        scale_factor = (
            1.0
            if scalable + unscalable <= 0.001 else
            (pop_ref - unscalable) / scalable
        )
        scale_map.values[msk] = scale_factor

    if iso in ["ISR", "GLP"]:
        print(f"{iso} scalable: {scalable:.15e}")
        print(f"{iso} unscalable: {unscalable:.15e}")
        print(f"{iso} gridded total: {scalable + unscalable:.15e}")
        print(f"{iso} target: {pop_ref:e}")
        print(f"{iso} scale factor: {scale_factor}")
        print(f"{iso} n_points: {msk_nz.sum()}")
        print("DDD")


def compute_scale_map(da_land_sea_mask, da_masks, da_pop, da_pop_ref):
    """Determine scaling factors for all grid cells to reproduce the reference populations

    Parameters
    ----------
    da_land_sea_mask, da_masks : xr.DataArray
        Output of `u_data.load_country_masks`.
    da_pop : xr.DataArray
        Output of `u_data.pop_hyde_unscaled`.
    da_pop_ref : xr.DataArray
        Output of `load_pop_ref`.

    Returns
    -------
    xr.DataArray with dimensions "lon" and "lat", corresponding to the grid in `da_pop`.
    """
    n_countries_per_gridcell = (da_masks > 0.0).sum(dim="iso")
    country_is_fractional = (da_masks < 1.0).all(dim=["lat", "lon"])
    n_fractional_countries = (da_masks.sel(iso=country_is_fractional) > 0).sum(dim="iso")
    n_grid_cells = (da_masks > 0.0).sum(dim=["lat", "lon"])

    print(f"{(n_grid_cells.values == 1).sum()} countries have only a single grid cell!")

    scale_map = xr.full_like(n_countries_per_gridcell, np.nan, dtype=np.float64)

    # rescale fractional countries with a single cell
    msk_ctry = country_is_fractional.values & (n_grid_cells.values == 1)
    for iso in da_masks["iso"].values[msk_ctry]:
        rescale_single_country(da_land_sea_mask, da_masks, da_pop, da_pop_ref, scale_map, iso)

    # rescale cells shared by ISR and PSE
    msk_nan = np.isnan(scale_map.values) & (n_fractional_countries.values > 1)
    msk_isr_pse = (
        (da_masks.sel(iso="ISR").values > 0.0)
        & (da_masks.sel(iso="PSE").values > 0.0)
    )
    scale_map.values[msk_nan & ~msk_isr_pse] = 1.0
    scale_map.values[msk_nan & msk_isr_pse] = 0.8

    # rescale fractional countries with more than a single cell, or "special countries"
    msk_ctry_special = np.isin(da_masks["iso"].values, ["BTN", "COG", "JOR", "KGZ", "ISR", "SVN"])
    msk_ctry = msk_ctry_special | (
        country_is_fractional.values & (n_grid_cells.values > 1)
    )
    for iso in da_masks["iso"].values[msk_ctry]:
        rescale_single_country(da_land_sea_mask, da_masks, da_pop, da_pop_ref, scale_map, iso)

    # shared grid cells that are not set to a scaling factor yet
    scale_map.values[
        (n_countries_per_gridcell.values > 1)
        & np.isnan(scale_map.values)
    ] = 1.0

    # rescale countries with at least one cell with weight 1.0
    msk_ctry = ~msk_ctry_special & ~country_is_fractional.values
    for iso in da_masks["iso"].values[msk_ctry]:
        rescale_single_country(da_land_sea_mask, da_masks, da_pop, da_pop_ref, scale_map, iso)

    # next, ensure that all cells with positive da_land_sea_mask or da_pop have scale_map not NaN
    msk = np.isnan(scale_map.values) & ((da_land_sea_mask.values > 0) | (da_pop.values > 0))
    scale_map.values[msk] = 0.0

    return scale_map


def rescale_pop(year, res_am, hyde_layer):
    """Rescale the HYDE population layer for a single year

    The result is written to a new NetCDF file in the output directory.

    Parameters
    ----------
    year : int
        The year for which to rescale data.
    res_am : int
        Resolution of the raster data in arc-minutes.
    hyde_layer : str
        One of "pop", "urb", or "rur".
    """
    out_path = u_const.OUTPUT_DIR / f"pop_{res_am:d}arcmin" / f"{hyde_layer}c_{year}.nc"
    if u_io.path_exists_safely(out_path):
        return

    da_pop = u_data.pop_hyde_unscaled(res_am, hyde_layer, year)
    da_pop_ref = load_pop_ref(year)
    da_land_sea_mask, da_masks = u_data.country_masks(res_am)

    # reindex da_masks to have a consistent ordering of countries between masks and reference pop
    da_pop_ref = da_pop_ref.sel(iso=np.isin(da_pop_ref["iso"].values, da_masks["iso"].values))
    da_masks = da_masks.reindex(iso=da_pop_ref["iso"].values)

    scale_map = compute_scale_map(da_land_sea_mask, da_masks, da_pop, da_pop_ref)
    da_result = (da_pop * scale_map).astype(da_pop.dtype)
    da_result.values[da_land_sea_mask.values == 0] = np.nan
    da_result.name = "total-population"
    u_io.write_safely(da_result.to_dataset(), out_path)


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    # note that only 5arcmin and 30arcmin (0.5 degrees) are implemented, and the only purpose of
    # 30arcmin is to compare our implementation of rescaling with the rescaled population data
    # included in ISIMIP3a.
    res_am = 5 if "5arcmin" in sys.argv[1:] else 30
    hyde_layer = (
        "rur" if "rur" in sys.argv[1:] else
        "urb" if "urb" in sys.argv[1:] else
        "pop"
    )

    period = u_const.ISIMIP3A_HIST_PERIOD
    l_years = np.arange(period[0], period[1] + 1)

    # 72 configurations
    year = l_years[task]
    rescale_pop(year, res_am, hyde_layer)


if __name__ == "__main__":
    main()
